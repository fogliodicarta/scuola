package cesare;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Cesare {

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		Scanner scan = new Scanner(System.in);
		Cripto k= new Cripto();
		DeCripto d = new DeCripto();
		char[] s = k.cripta(scan.nextLine());
		//char[] s = k.cripta("zciaoz");
		
		System.out.println(s);
		System.out.println(k.getSliding());
		
		System.out.println(d.decripta(s));
	}
	
}
