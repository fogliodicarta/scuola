package cesare;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;

public class Cripto {
	private int spostamento;
	private char[] criptata;

	public Cripto() {
		Random rand = new Random();
		spostamento = (int) Math.floor(rand.nextInt(5) + 1);
	}

	public char[] cripta(String inChiaro) throws FileNotFoundException, UnsupportedEncodingException {
		char[] cripted = inChiaro.toCharArray();
		for (int i = 0; i < cripted.length; i++) {
			cripted[i] = (char) (cripted[i] + spostamento);

			if ((int) (cripted[i]) > (int) 'z') {
				cripted[i] = (char) (cripted[i] - ('z' + 1 - 'a'));
			}
		}
		criptata = cripted.clone();

		PrintWriter writer = new PrintWriter("chiave.txt", "UTF-8");
		writer.println(spostamento);
		writer.close();

		return cripted;
	}

	public int getSliding() {
		return spostamento;
	}
}
