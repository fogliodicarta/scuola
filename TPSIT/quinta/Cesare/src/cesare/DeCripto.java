package cesare;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DeCripto {
	public char[] decripta(char[] criptata) throws FileNotFoundException {
		File file = new File("chiave.txt");
		Scanner s = new Scanner(file);	
		int spostamento = s.nextInt();
		s.close();
		for (int i = 0; i < criptata.length; i++) {
			criptata[i] = (char) (criptata[i] - spostamento);

			if ((int) (criptata[i]) < (int) 'a') {
				criptata[i] = (char) (criptata[i] + ('z' + 1 - 'a'));
			}
		}
		return criptata;
	}
}
