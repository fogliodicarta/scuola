package com.example.myboton;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText raggio;
    TextView area;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        raggio = (EditText) findViewById(R.id.raggio);
        area = (TextView) findViewById(R.id.area);
    }

    public void clickBottone(View view){
       int r= Integer.parseInt(raggio.getText().toString());
       float S= (float) (r*r*3.14);
       area.setText(String.format("%.2f",S));
    }
}
