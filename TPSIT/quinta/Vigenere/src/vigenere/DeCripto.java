package vigenere;

public class DeCripto {
	private String worm;
    
	private final static String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwyxz +-,.!?éèòàùì";

	public DeCripto(String worm) {
		this.worm=worm;
	}
	
	public String decripta(String mex){
		StringBuilder decriptata = new StringBuilder(mex);
		for (int i = 0; i < mex.length(); i++) {

			int sommaInt = alfabeto.indexOf(mex.charAt(i))-alfabeto.indexOf(worm.charAt(i%worm.length()));
			
			sommaInt=(sommaInt > 0)?sommaInt:sommaInt+alfabeto.length();
			char sommaChar = alfabeto.charAt(sommaInt);
			decriptata.setCharAt(i, sommaChar);
		}
		return decriptata.toString();
		
	}
}
