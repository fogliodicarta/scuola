package vigenere;

import java.util.Scanner;

public class Vigenere {

	public static void main(String[] args) {
		Cripto k = new Cripto("VERME");
		DeCripto dk = new DeCripto("VERME");
		Scanner tast  = new Scanner(System.in);
		String messaggio = tast.nextLine();
		String s = k.cripta(messaggio);
		System.out.println(s);
		s=dk.decripta(s);
		System.out.println(s);
	}	
}
