package vigenere;

public class Cripto {
	private final static String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWYXZabcdefghijklmnopqrstuvwyxz +-,.!?éèòàùì";
	private String worm;
	
	private int at(char c){
		return alfabeto.indexOf(c);
	}
	
	Cripto(String worm){
		this.worm = worm;
	}
	
	public String cripta(String mex){
		StringBuilder criptata = new StringBuilder(mex);
		for (int i = 0; i < mex.length(); i++) {

			int sommaInt = alfabeto.indexOf(mex.charAt(i))+alfabeto.indexOf(worm.charAt(i%worm.length()));
			
			sommaInt=(sommaInt < alfabeto.length())?sommaInt:sommaInt-alfabeto.length();
			char sommaChar = alfabeto.charAt(sommaInt);
			criptata.setCharAt(i, sommaChar);
		}
		return criptata.toString();
	}
}
