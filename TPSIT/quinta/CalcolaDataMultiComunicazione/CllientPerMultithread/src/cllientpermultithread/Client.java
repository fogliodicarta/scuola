package cllientpermultithread;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	Socket socket;
	ObjectOutputStream out;
	ObjectInputStream in;
	private final String crittografia="chiave";
	Client(){
		try {
			socket= new Socket("localhost",32222);
			out=new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException ex) {
			System.out.println("fallita creazione socket");
		}
	}
	public void invia(){
		try {
			String server_msg = (String) in.readObject();
			System.out.println(server_msg);
			Scanner tast = new Scanner(System.in);
			out.writeObject(tast.nextLine());
			String eta = (String) in.readObject();
			System.out.println(eta);
			
		} catch (IOException | ClassNotFoundException ex) {
			System.out.println("errore invio");
		}
	}
	public void close() throws IOException {
		out.close();
		in.close();
		socket.close();
	}
}
