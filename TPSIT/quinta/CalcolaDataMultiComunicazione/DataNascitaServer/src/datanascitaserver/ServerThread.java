package datanascitaserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ServerThread implements Runnable {
	private Socket client = null;
	public String clientIP = null;

	public ServerThread(Socket client) {
		this.client = client;
		clientIP = client.getInetAddress().getHostAddress();
	}

	@Override
	public void run() {
		ObjectOutputStream out = null;
		ObjectInputStream in = null;
		try {//	CREA GLI STREAMS
			out = new ObjectOutputStream(client.getOutputStream());
			out.flush();
			in = new ObjectInputStream(client.getInputStream());
			System.out.println("Stream creati");

			String mex = "";
			/*out.writeObject("SERVER -> Ciao digita BYE per terminare...");//INVIA IL MESSAGGIO DI INIZIO AL CLIENT
			out.flush();*/

			do {
				out.writeObject("Dimmi la data di nascita in gg/mm/aaaa");
				mex = (String) in.readObject();
				System.out.println(clientIP.toString() + " : " + mex);
				out.writeObject(Integer.toString(DateToAge(mex),10));
				out.flush();
			} while (!mex.trim().equals("BYE"));
		} catch (Exception e) {
			System.out.println("comunicazione interrotta");
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
				client.close();
			} catch (IOException ex) {
				System.out.println("Fallita chiusura socket");
			}

			System.out.println("fine connessione con: [" + clientIP + "]");
		}
	}
	public int DateToAge(String dataNascita){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");  
	    LocalDateTime now = LocalDateTime.now();  
		String oggi=dtf.format(now);
        //System.out.println(dtf.format(now));  
		int doggi = Integer.parseInt(oggi.substring(0, 2));
		int moggi = Integer.parseInt(oggi.substring(3,5));
		int yoggi = Integer.parseInt(oggi.substring(6));
		
		int dieri= Integer.parseInt(dataNascita.substring(0, 2));
		int mieri = Integer.parseInt(dataNascita.substring(3,5));
		int yieri = Integer.parseInt(dataNascita.substring(6));
		
		int diffAnni=yoggi-yieri;
		int diffMesi=moggi-mieri;
		int diffGiorni=doggi-dieri;
		
		if (diffMesi<0||(diffMesi==0&&diffGiorni<0)) {
			diffAnni--;
		}
		return diffAnni;
	}
}
