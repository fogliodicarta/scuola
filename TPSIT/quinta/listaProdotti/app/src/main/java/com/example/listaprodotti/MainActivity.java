package com.example.listaprodotti;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String end="\r\n";
    EditText user;
    DatePicker calendario;
    RadioGroup radios;
    TextView riepilogo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        user = findViewById(R.id.username);
        calendario = findViewById(R.id.calendario);
        radios=findViewById(R.id.radios);
        riepilogo = findViewById(R.id.riepilogo);
    }

    public void elabora(View v){
        int selectedId=radios.getCheckedRadioButtonId();
        RadioButton rd=findViewById(selectedId);

        String s="username:"+user.getText().toString()+end+
                "Data:"+calendario.getDayOfMonth()+"/"+(calendario.getMonth()+1)+"/"+calendario.getYear()+end+
                "Prodotto:"+ rd.getText().toString()+end;
        riepilogo.setText(s);
    }
}
