package com.example.eurodollaro;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DecimalFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    final double conv = 1.1;

    DecimalFormat df = new DecimalFormat("#.##");

    EditText editEuro;
    EditText editDollaro;
    Button buttonEuro;
    Button buttonDollaro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editEuro= (EditText) findViewById(R.id.editEuro);
        editDollaro = (EditText) findViewById(R.id.editDollaro);
         buttonEuro = (Button) findViewById(R.id.buttonEuro);
        buttonDollaro= (Button) findViewById(R.id.buttonDollaro);
        buttonEuro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double eur = Double.parseDouble(editEuro.getText().toString());
                // Log.w("euri",Float.toString(eur));
                double usd = eur* conv;
                editDollaro.setText(String.format(Locale.US, "%.2f", usd));
            }
        });
        buttonDollaro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double usd = Double.parseDouble(editDollaro.getText().toString());
                // Log.w("euri",Float.toString(eur));
                double eur = usd/conv;
                editEuro.setText(String.format(Locale.US, "%.2f", eur));
            }
        });
    }
}
