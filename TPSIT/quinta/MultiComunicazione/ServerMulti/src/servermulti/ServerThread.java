package servermulti;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerThread implements Runnable {
	private Socket client = null;
	private String clientIp = null;

	public ServerThread(Socket client) {
		this.client = client;
		clientIp = this.client.getInetAddress().getHostAddress();
		System.out.println("[" + clientIp + "] " + ">> Connessione in ingresso <<");
	}

	@Override
	public void run() {
		ObjectOutputStream out = null;
		ObjectInputStream in = null;
		try {//	CREA GLI STREAMS
			out = new ObjectOutputStream(client.getOutputStream());
			out.flush();
			in = new ObjectInputStream(client.getInputStream());
			System.out.println("Stream creati");

			String mex = "";
			out.writeObject("SERVER -> Ciao digita BYE per terminare...");//INVIA IL MESSAGGIO DI INIZIO AL CLIENT
			out.flush();

			do {
				mex = (String) in.readObject();
				System.out.println(clientIp.toString()+" : " + mex);
				out.writeObject("echo: " + mex);
				out.flush();
			} while (!mex.trim().equals("BYE"));
		} catch (Exception e) {
			System.out.println("comunicazione interrotta");
		}finally
		{
			try
			{
				if (out != null) out.close();
				if (in  != null) in.close();
				client.close();
			}
			catch(IOException ex)
			{
				System.out.println("Fallita chiusura socket");
			}		

			System.out.println("fine connessione con: [" + clientIp + "]");
		}
	}

}
