package servermulti;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMulti {

	public static void main(String[] args) {
		ServerSocket server_socket = null;
		try {
			server_socket = new ServerSocket(32222);
		} catch (IOException ex) {
			System.out.println("Fallita creazione server socket");
		}

		while (true) {
			try {
				Socket socket = server_socket.accept(); // pongo il server in attesa di connessioni
				ServerThread serverThread = new ServerThread(socket);
				Thread worker = new Thread(serverThread);
				worker.start();
			} catch (IOException ex) {
				System.out.println("sei un fallito");
			}
		}
	}

}
