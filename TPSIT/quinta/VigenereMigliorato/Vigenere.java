package criptoserver;
public class Vigenere {
	public static final String dizionario = "abcdefghijklmnopqrstuvwxyz .,";
	public static String cripta(String s,String chiave){
		String criptata="";
		for (int is = 0,ichiave=0; is < s.length(); is++,ichiave++) {
			ichiave = ichiave%chiave.length();
			criptata+=addLettera(s.charAt(is),chiave.charAt(ichiave));
		}
		return criptata;
	}
	public static String decripta(String s,String chiave){
		String decriptata="";
		for (int is=0,ichiave=0;is<s.length();is++,ichiave++){
			ichiave = ichiave%chiave.length();
			decriptata+=subLettera(s.charAt(is),chiave.charAt(ichiave));
		}
		return decriptata;
	}
	public static char addLettera(char c1,char c2){
		int sommaIndici = dizionario.indexOf(c1)+dizionario.indexOf(c2);
		sommaIndici= sommaIndici>=dizionario.length()?sommaIndici-dizionario.length():sommaIndici;
		return dizionario.charAt(sommaIndici);
	}
	public static char subLettera(char c1,char c2){
		int subIndici =  dizionario.indexOf(c1)-dizionario.indexOf(c2);
		subIndici = subIndici < 0 ? subIndici+dizionario.length():subIndici;
		return dizionario.charAt(subIndici);
	}
}
