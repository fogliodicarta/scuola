package criptoclient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


public class Client {
	Socket socket;
	ObjectOutputStream out;
	ObjectInputStream in;
	private final String crittografia="chiave";
	Client(){
		try {
			socket= new Socket("localhost",32222);
			out=new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException ex) {
			System.out.println("fallita creazione socket");
		}
	}
	public void invia(String mexInvio){
		try {
			out.writeObject(Vigenere.cripta(mexInvio,crittografia));
			String server_msg = (String) in.readObject();
			System.out.println("echo dal server:"+Vigenere.decripta(server_msg,crittografia));
		} catch (IOException | ClassNotFoundException ex) {
			System.out.println("errore invio");
		}
	}
	public void close() throws IOException {
		out.close();
		in.close();
		socket.close();
	}
}
