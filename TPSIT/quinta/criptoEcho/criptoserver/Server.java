package criptoserver;


import java.io.IOException;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	ServerSocket server;
	Socket socketVero;
	ObjectOutputStream out;
	ObjectInputStream in;
	private final String crittografia="chiave";
	Server(){
		try {
			server = new ServerSocket(32222);
		} catch (IOException ex) {
			System.out.println("non riesco a creare il socket");
		}
	}
	public void ricevi() throws ClassNotFoundException, IOException {
		try {
			socketVero=server.accept();
			out = new ObjectOutputStream(socketVero.getOutputStream());
			in = new ObjectInputStream(socketVero.getInputStream());
			System.out.println("Stream creati");
			while(true){
			  String mexRicevuto=(String) in.readObject();
			  System.out.println(Vigenere.decripta(mexRicevuto,crittografia));
			  out.writeObject(mexRicevuto);
			}

		} catch (IOException ex) {
			System.out.println("connessione terminata");
			System.out.println("----------------------------------------------------------");
			in.close();
			out.close();
			socketVero.close();
			server.close();

		}
	}
	
}
