#include <stdio.h>
void calcolafrequenze(char * str,int * fr){
  for(int i=0;str[i]!='\0';i++){
    int c = str[i];
    c-= (c>=97) ? 97 : 65;
    fr[c]++; 
  }
}
int main(){
  int freq[26]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  char s[64];
  scanf("%[^\n]s",s);
  calcolafrequenze(s,freq); 
  for (int i=0;i<26;i++) {
    if (freq[i] > 0) {
      printf("%c %d\n",'A'+i,freq[i]);
    }
  }
}
