#include <stdio.h>
#include <math.h>
#include <string.h>

int hextodec(char * esa){
  int lung=strlen(esa);
  int potenza=lung-1;
  int i=0;
  int decimale=0;
  int valido=1;
  while(i<lung && valido){
    int c = esa[i];
    if(c>='a'&&c<='f'){
      c-=87;
    }else if(c>='A'&&c<='F'){
      c-=55;
    }else if(c>='0'&&c<='9'){
      c-=48;
    }else{
      valido=0;
    }
    decimale+=c*pow(16,potenza);
    i++;
    potenza--;
  }
  return valido ? decimale : -1;
}

int main(){
  char s[16];
  scanf("%s",s);
  int n = hextodec(s);
  printf("%d",n);
}
