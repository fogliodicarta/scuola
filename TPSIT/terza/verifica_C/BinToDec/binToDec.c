#include <stdio.h>
#include <math.h>
#include <string.h>

int bintodec(char * bin){
  int lung=strlen(bin);
  int potenza=lung-1;
  int i=1;
  int c = bin[0]-48;
  int decimale=(c*pow(2,potenza))*-1;
  potenza--;
  while(i<lung){
    c = bin[i];
    c-=48;
    decimale+=c*pow(2,potenza);
    i++;
    potenza--;
  }
  return decimale;
}

int main(){
  char s[16];
  scanf("%s",s);
  int n = bintodec(s);
  printf("%d",n);
}
