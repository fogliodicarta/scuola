//INTERPRETE UMLP IN C++
#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
#include <math.h>
using namespace std;
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
char codiceOperativo[][10]= {"add","div","halt","jmp","jmpeq","jmpge","jmpgt","jmple","jmplt","jmpne","mod","mul","neg","newline","pop","print","printvar","push","pushvar","read","rem","srt","store","sub"}; //I COMANDI VANNO IN CAPS
const int nElementi=24;
int getCodiceOperativoBinaria(char * cop)     //BINARIA
{
    int basso=0, mezzo, alto=nElementi-1;
    int compare;
    while(basso<=alto)
    {
        mezzo=(basso+alto)/2;
        compare=strcmp(cop,codiceOperativo[mezzo]);
        if(compare<0)
        {
            alto=mezzo-1;
        }
        else if(compare>0)
        {
            basso=mezzo+1;
        }
        else
        {
            return mezzo;
        }
    }
    return -1;
}
/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////STACK///////////////////////////////////////
int stackAtt[300];//STACK
int sp=-1;//STACK POINTER
int topRegister=0;
char arg[50];
/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////MEMORIA PROGRAMMA///////////////////////////
int posCodOps;                 //PROGRAM COUNTER
char param[100][50];    //INSTRUCTION MEMORY:ARGUMENT
int codops[100];      //INSTRUCTION MEMORY:OPERATIVE CODE
bool interrotto=false;
/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////MEMORIA VARIABILI///////////////////////////
char nomeVariabili[512][16];
int valoreVariabili[512];
int nVariabili=0;
int getIndice(char * name)
{
    int i;
    for(i=0; i<nVariabili && strcmp(name,nomeVariabili[i]); i++);
    //cout << "INDICE VARIABILE: " <<i<<endl;
    return i<nVariabili ? i :-1;
}
void mVar()
{
    for(int i=0; i<=nVariabili; i++)
    {
        printf("%s%c%d\n",nomeVariabili[i],':',valoreVariabili[i],'\n');
    }
}
/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////OPERAZIONI SULLO STACK//////////////////////
void top()
{
    if(sp>=0)
    {
        topRegister=stackAtt[sp];
    }
    else
    {
        printf("%s","TOP IMPOSSIBILE\n");
    }

}
void pop()
{
    if(sp>=0)
    {
        top();
        sp--;
    }
    else
    {
        printf("%s","POP IMPOSSIBILE\n");
    }
}
void push()
{
    sp++;
    stackAtt[sp]=atoi(arg);
}
void pushvar()
{
    int indice = getIndice(arg);
    sp++;
    stackAtt[sp]=valoreVariabili[indice];
}
void store()
{
    int indice = getIndice(arg);
    if(indice==-1)
    {
        strcpy(nomeVariabili[nVariabili],arg);
        valoreVariabili[nVariabili]=stackAtt[sp];
        nVariabili++;
    }
    else
    {
        valoreVariabili[indice]=stackAtt[sp];
    }
}

/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////OPERAZIONI MATEMATICHE//////////////////////

void add()
{
    pop();
    int temp=topRegister;
    pop();
    itoa(temp+topRegister,arg,10);
    push();
}
void sub()
{
    pop();
    int temp=topRegister;
    pop();
    itoa(topRegister-temp,arg,10);
    push();
}
void mul()
{
    pop();
    int temp=topRegister;
    pop();
    itoa(topRegister*temp,arg,10);
    push();
}
void div()
{
    pop();
    int temp=topRegister;
    pop();
    itoa(topRegister/temp,arg,10);
    push();
}
void mod()
{
    pop();
    int temp=topRegister;
    pop();
    itoa(topRegister%temp,arg,10);
    push();
}
void neg()
{
    pop();
    int temp =topRegister;
    itoa(0-temp,arg,10);
    push();
}
void srt()
{
    pop();
    itoa( floor(sqrt(topRegister)), arg,10);
    push();
}
/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////ALTRE OPERAZIONI////////////////////////////
void jmp()
{
    posCodOps = atoi(arg)-1;
}
void jmpeq()
{
    top();
    if(topRegister==0)
    {
        posCodOps=atoi(arg)-1;
    }
}
void jmpge()
{
    top();
    if(topRegister>=0)
    {
        posCodOps=atoi(arg)-1;
    }
}
void jmpgt()
{
    top();
    if(topRegister>0)
    {
        posCodOps=atoi(arg)-1;
    }
}
void jmple()
{
    top();
    if(topRegister<=0)
    {
        posCodOps=atoi(arg)-1;
    }
}
void jmplt()
{
    top();
    if(topRegister<=0)
    {
        posCodOps=atoi(arg)-1;
    }
}
void jmpne()
{
    top();
    if(topRegister!=0)
    {
        posCodOps=atoi(arg)-1;
    }
}
void halt()
{
    interrotto=true;
}

void print()
{
    printf("%s",arg);
}
void printvar()
{
    int indice = getIndice(arg);
    if(indice !=-1)
    {
        printf("%s%c%d\n",nomeVariabili[indice],':',valoreVariabili[indice],'\n');

    }

}

void newline()
{
    printf("\n",'\n');
}
void read()
{
    int valore;
    scanf("%d",&valore);
    int indice = getIndice(arg);
    if(indice==-1)
    {
        strcpy(nomeVariabili[nVariabili],arg);
        valoreVariabili[nVariabili]=valore;
        nVariabili++;
    }
    else
    {
        valoreVariabili[indice]=valore;
    }


}
void write(char *name)
{
    int indice = getIndice(name);
    printf("%d",valoreVariabili[indice]);
}
void rem()
{
    printf("%d",stackAtt[sp]);
}

/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////////////////////////////////////////////////
void (*funzioni[24])()= {add,div,halt,jmp,jmpeq,jmpge,jmpgt,jmple,jmplt,jmpne,mod,mul,neg,newline,pop,print,printvar,push,pushvar,read,rem,srt,store,sub};
/*///////////////////////////////////////////////////////////////////////////
*////////////////////////////////LETTURA FILE////////////////////////////////
void rimuoviHashtags(char * stringa)
{
    int i;
    for(i=0; stringa[i]!='#'; i++);
    stringa[i]='\0';

}

int main()
{
    FILE * fin;
    fin = fopen("umlp.txt","r");
    int lunghezzatxt=100;
    char linea[lunghezzatxt]="";
    posCodOps=0;
    while((fgets(linea,sizeof(linea),fin)))
    {
        char comando[lunghezzatxt];
        char argomento[lunghezzatxt];
        bool commento=false;

        int posLinea=0;
        int posComArg=0;
        while(linea[posLinea]==' ') posLinea++; //PER SALTARE GLI SPAZI
        if(linea[posLinea]!='\n')
        {
            do//PER LEGGERE IL COMANDO
            {
                commento = commento || linea[posLinea]=='#';
                comando[posComArg]=linea[posLinea];
                posLinea++;
                posComArg++;
            }
            while(linea[posLinea]!=' '&& linea[posLinea]!='\n');

            comando[posComArg]='\0';
            if(commento)
            {
                rimuoviHashtags(comando);
            }


            posComArg=0;
            while(linea[posLinea]==' ') posLinea++; //PER SALTARE GLI SPAZI


            if(linea[posLinea]!='\n'&&linea[posLinea]!='\0'&&!commento) //DA FARE SOLO SE C'� L'ARGOMENTO
            {
                do//PER LEGGERE L'ARGOMENTO
                {
                    commento = commento || linea[posLinea]=='#';
                    argomento[posComArg]=linea[posLinea];
                    posLinea++;
                    posComArg++;
                }
                while(linea[posLinea]!=' '&& linea[posLinea]!='\n');
                //PRIMA QUI C'ERA argomento[posComArg]='\0';
                if(commento)
                {
                    rimuoviHashtags(argomento);
                }
                argomento[posComArg]='\0';
            }

            codops[posCodOps]=getCodiceOperativoBinaria(comando);
            if (codops[posCodOps]==-1)
            {
                printf("%s%d%s%s%c\n","ERRORE: COMANDO SCONOSCIUTO ALLA RIGA:",posCodOps," \"",comando,'\"','\n');//RICORDA DI CHIEDERE SE METTERE  posCodOps+1
                return 1;
            }
            strcpy(param[posCodOps],argomento);
            posCodOps++;
        }
    }

     codops[posCodOps]=nElementi;//COME FINE DEL CICLO DI INTERPRETAZIONE/ESECUZIONE C'� IL NUMERO DI ELEMENTI DELL'ARRAY CODICIOPERATIVI
    /*///////////////////////////////////////////////////////////////////////////
    *//////////////////////////INTEPRETAZIONE ED ESECUZIONE//////////////////////
    posCodOps=0;

    while(codops[posCodOps]!=nElementi && !interrotto)
    {
        int comando;
        comando=codops[posCodOps];
        strcpy(arg,param[posCodOps]);
        funzioni[comando]();
        posCodOps++;
    }
    return 0;
}
