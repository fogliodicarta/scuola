#!/bin/bash
## INTERPRETE UMLP PER UN MICRO LINGUAGGIO MACCHINA (A STACK) - Versione 1.0 da testare - G.C. 16-02-2018
# N.B. Nella versione 1.0 l'arg di print si pu� mettere tra "" e si possono usare escapes come \n \t o \"


#########################################   IMPLEMENTAZIONE STACK DI VALUTAZIONE
declare -a stack=()     # memoria dello stack, inizialmente vuota
declare sp=-1  		# stack pointer
declare topRegister=0
declare arg

function pop(){
  if [ $sp -ge 0 ] ;then
   top
   ((sp--))
  else
    errCode=1
    errMess="pop impossibile"
  fi
}


function push(){
  ((sp++))
  stack[$sp]=$arg
}


function pushvar(){
  ((sp++))
  #echo $arg
  stack[$sp]=${!arg}  #accesso indiretto in lettura
  #echo ${stack[$sp]}

}

function store(){
  #echo $arg
  eval "$arg=\${stack[$sp]}" #accesso indiretto in scrittura
}

function top(){
  if [ $sp -ge 0 ] ;then
     topRegister=${stack[$sp]}
  else
    echo "top impossibile"
  fi
}

#################################  IMPLEMENTAZIONE DI ALCUNE ISTRUZIONI

function add() {
   pop
   local x=$topRegister
   #echo $x
   pop
   arg=$(($topRegister+$x))
   push
}  
  
function sub() {
   pop
   local x=$topRegister
   pop
   arg=$(($topRegister-$x))
   push
}

function mul() {
   pop
   local x=$topRegister
   pop
   arg=$(($topRegister*$x))
   push
}

function div() {
   pop
   local x=$topRegister
   pop
   arg=$(($topRegister/$x))
   push
}

function mod() {
   pop
   local x=$topRegister
   pop
   arg=$(($topRegister%$x))
   push
}

function neg() {
   pop
   local x=$topRegister
   arg=$((0-$x))
   push
}

function sqrt() {
   pop
   local x=$topRegister
   arg=$( echo $x | awk  '{ print int(sqrt($1)); }') 
   #arg=$(echo $x | ./sqrt ) # alternativa se esiste un programma sqrt nella dir corrente
   push
}
#############################################   MAIN DI INTERPRETAZIONE

declare errCode=0

if [ $# -eq 0 ] ; then
   echo "manca il nome del file argomento"
   errCode=-1
elif ( ! [ -a $1 ] ); then
   echo "il file $1 non esiste"
   errCode=-2
else

  declare linea
 
  while  read -r linea   
  do
    if [ $errCode -ne 0 ] ; then 
       break
    fi 
    #echo "$linea"
    comando=$( echo $linea  | awk '{ print $1 }' )  #uso di awk per isolare il nome del comando 
    arg=$( echo $linea  | awk '{ print $2 }' )      #uso di awk per isolare l'eventuale argomento del comando
    case $comando in
      newline) echo "";;    
      print)   { 
                  arg1=$(echo $linea|awk        ' /\".*\"$/ { sarg="\1"; start=index($0,"\"");
                                                         for (i=start+1;i<length($0);i++) {      
                                                           sarg=sarg  substr($0,i,1);
                                                         } 
                                                         print(sarg);}       
                                                  $0 !~/\"/  { print($2); }') #se non si usa "....." 
                  echo -ne $arg1
               }
             ;;
      read)    {
                read  xx </dev/tty # necessario altrimenti legge da $1
                eval "$arg=\$xx"   #accesso indiretto in scrittura
          };;  
      printvar) echo -n ${!arg} ;;  #accesso indiretto in lettura
      pop)     pop ;;
      push)    push;;
      pushvar) pushvar;;
      store)   store;;
      add)     add;;
      sub)     sub;;
      neg)     neg;;
      mul)     mul;;
      div)     { top;
                 if [ $topRegister -eq 0 ] ; then
                      errCode=4
                      errMess="Divisione per zero" 
                 else 
                   div
                 fi
               };;
      mod)     { top;
                 if [ $topRegister -eq 0 ] ; then
                      errCode=5
                      errMess="Resto di una divisione per zero" 
                 else 
                   mod   #resto della divisione
                 fi
               };;
      sqrt)    { top;
                 if [ $topRegister -lt 0 ] ; then
                      errCode=6
                      errMess="Radice di un numero negativo: $topRegister"
                 else 
                   sqrt   #radice quadrata
                 fi
               };;
      "")   ;;  #se leggi una linea vuota non fare niente
      rem) ;;  #commento (da ignorare per l'esecuzione )
      *) {
            errMess="$comando : istruzione sconosciuta"
            errCode=3
         }
       ;;

            
    esac
  done < "$1"  #leggi dal file specificato come argomento
fi
if [ $errCode -gt 0 ]; then
   echo -e "\nRUN TIME ERROR: $errMess"
fi
exit $errCode
