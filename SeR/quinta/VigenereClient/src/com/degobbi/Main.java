package com.degobbi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    private static Socket s;
    private static ObjectInputStream inputStream;
    private static ObjectOutputStream outputStream;

    private static String mandaStringa(String messaggio){
        String risposta="";

        try {
            outputStream.writeObject(messaggio);
            outputStream.flush();
            risposta = (String) inputStream.readObject();
        }catch (Exception e){
            System.out.println("errore nell'invio/ricezione");
        }

        return risposta;
    }

    public static void main(String[] args) {
        //System.out.println(Vigenere.cripta("aaabcdef"));

        try {//crea socket e streams
            s = new Socket("localhost",3000);
            outputStream = new ObjectOutputStream(s.getOutputStream());
            outputStream.flush();
            inputStream = new ObjectInputStream(s.getInputStream());

            System.out.println("flushato");
        } catch (IOException e) {
            System.out.println("errore creazione del socket/stream");
        }
        try {//ricevi messaggio di benvenuto
            System.out.println(inputStream.readObject());
        }catch (Exception e){
            System.out.println("errore ricezione messaggio iniziale");
        }
        Scanner tastiera = new Scanner(System.in);

        do {//leggo l'input lo invio e printo la risposta
            String messaggio=tastiera.nextLine();
            String risposta=mandaStringa(Vigenere.cripta(messaggio));
            System.out.println(risposta);
        }while(true);


    }
}
