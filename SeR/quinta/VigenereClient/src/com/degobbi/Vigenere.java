package com.degobbi;

public class Vigenere {
    private final static String alfabeto="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,.!?;:";
    private final static String worm="VERME";

    public static String cripta(String chiaroString){
        char[] risultato=chiaroString.toCharArray(); //converte a char array
        for (int i=0;i<risultato.length;i++){
            if(risultato[i]!=' ') { //se è uno spazio lo lascia
                int indiceSomma = alfabeto.indexOf(risultato[i]) + alfabeto.indexOf(worm.charAt(i % worm.length()));
                indiceSomma = indiceSomma % alfabeto.length(); //faccio il modulo in caso superi la lunghezza di alfabeto
                risultato[i] = alfabeto.charAt(indiceSomma);
            }
        }
        return new String(risultato);
    }
}
