package com.example.bluetooth1;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Set;
import java.util.UUID;

public class colorPick extends AppCompatActivity {
    LinearLayout llCerchio;
    SeekBar sbTrasparenza,sbRosso,sbVerde,sbBlu;
    EditText editR,editG,editB;

    String formatoInvio="%03d,%03d,%03d";

    int r=0;
    int g=0;
    int b=0;
    int hue=100;

    UUID SPP_UUID;
    BluetoothAdapter bluetoothAdapter;
    BluetoothDevice targetDevice;
    int ConnessioneOK = -1;
    BluetoothSocket btSocket;
    OutputStreamWriter writer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_pick);

        llCerchio = (LinearLayout) findViewById(R.id.llCerchio);
        sbTrasparenza = (SeekBar) findViewById(R.id.sbTrasparenza);
        sbRosso = (SeekBar) findViewById(R.id.sbRosso);
        sbVerde = (SeekBar) findViewById(R.id.sbVerde);
        sbBlu = (SeekBar) findViewById(R.id.sbBlu);

        editR = (EditText) findViewById(R.id.editR);
        editG = (EditText) findViewById(R.id.editG);
        editB = (EditText) findViewById(R.id.editB);

        Log.w("nome",prendiNome());

        editR.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    int temp = Integer.parseInt(editR.getText().toString());
                    if (temp<256 && temp >=0){
                        r=temp;
                        GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                        String hex = String.format("#%02x%02x%02x", r, g, b);
                        myCircle.setColor(Color.parseColor(hex));
                        sbRosso.setProgress(r);

                    }else{
                        editR.setText("0", TextView.BufferType.EDITABLE);
                        r=0;
                        GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                        String hex = String.format("#%02x%02x%02x", r, g, b);
                        myCircle.setColor(Color.parseColor(hex));
                    }
                    inviaBluetooth(String.format(formatoInvio,r,g,b));
                    return true;
                }
                return false;
            }
        });

        editG.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    int temp = Integer.parseInt(editG.getText().toString());
                    if (temp<256 && temp >=0){
                        g=temp;
                        GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                        String hex = String.format("#%02x%02x%02x", r, g, b);
                        myCircle.setColor(Color.parseColor(hex));
                        sbVerde.setProgress(g);
                    }else{
                        editG.setText("0",TextView.BufferType.EDITABLE);
                        g=0;
                        GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                        String hex = String.format("#%02x%02x%02x", r, g, b);
                        myCircle.setColor(Color.parseColor(hex));
                    }
                    inviaBluetooth(String.format(formatoInvio,r,g,b));
                    return true;
                }
                return false;
            }
        });

        editB.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    int temp = Integer.parseInt(editB.getText().toString());
                    if (temp<256 && temp >=0){
                        b=temp;
                        GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                        String hex = String.format("#%02x%02x%02x", r, g, b);
                        myCircle.setColor(Color.parseColor(hex));
                        sbBlu.setProgress(b);
                    }else{
                        editB.setText("0",TextView.BufferType.EDITABLE);
                        b=0;
                        GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                        String hex = String.format("#%02x%02x%02x", r, g, b);
                        myCircle.setColor(Color.parseColor(hex));
                    }
                    inviaBluetooth(String.format(formatoInvio,r,g,b));
                    return true;
                }
                return false;
            }
        });



        sbTrasparenza.setOnSeekBarChangeListener(seekBarChangeListenerTrasp);
        sbRosso.setOnSeekBarChangeListener(seekBarChangeListenerRosso);
        sbVerde.setOnSeekBarChangeListener(seekBarChangeListenerVerde);
        sbBlu.setOnSeekBarChangeListener(seekBarChangeListenerBlu);

        setUpBluetooth();

    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerTrasp =
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    myCircle.setAlpha(progress);
                    hue=progress;
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    String mexaggio;
                    mexaggio=String.format(formatoInvio,getReduced(r),getReduced(g),getReduced(b));
                    inviaBluetooth(mexaggio);
                }

            };

    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerRosso=
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    r=progress;
                    String hex = String.format("#%02x%02x%02x", r, g, b);
                    myCircle.setColor(Color.parseColor(hex));
                    editR.setText(String.format("%d",progress), TextView.BufferType.EDITABLE);


                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    String mexaggio;
                    mexaggio=String.format(formatoInvio,getReduced(r),getReduced(g),getReduced(b));
                    inviaBluetooth(mexaggio);
                }

            };

    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerVerde=
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    g=progress;
                    String hex = String.format("#%02x%02x%02x", r, g, b);
                    myCircle.setColor(Color.parseColor(hex));
                    editG.setText(String.format("%d",progress), TextView.BufferType.EDITABLE);

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    String mexaggio;
                    mexaggio=String.format(formatoInvio,getReduced(r),getReduced(g),getReduced(b));
                    inviaBluetooth(mexaggio);
                }

            };
    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerBlu=
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    GradientDrawable myCircle = (GradientDrawable) llCerchio.getBackground();
                    b=progress;
                    String hex = String.format("#%02x%02x%02x", r, g, b);
                    myCircle.setColor(Color.parseColor(hex));
                    editB.setText(String.format("%d",progress), TextView.BufferType.EDITABLE);

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    String mexaggio;
                    mexaggio=String.format(formatoInvio,getReduced(r),getReduced(g),getReduced(b));
                    inviaBluetooth(mexaggio);
                }

            };
    public String prendiNome(){
        Intent in = getIntent();
        return in.getStringExtra("arduino");
    }
    public void setUpBluetooth(){
        String deviceName = prendiNome();
        SPP_UUID=java.util.UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        targetDevice = null;
        for(BluetoothDevice pairedDevice : pairedDevices)
            if(pairedDevice.getName().equals(deviceName)) {
                targetDevice = pairedDevice;
                break;
            }

        // se il dispositivo non viene trovato nella lista viene segnalato l'errore
        if(targetDevice == null) {
            Toast.makeText(this, "Dispositivo non trovato", Toast.LENGTH_SHORT).show();
            ConnessioneOK=1;
            return;
        }

        // crea un socket di connessione con il dispositivo
        btSocket = null;
        try {
            btSocket = targetDevice.createInsecureRfcommSocketToServiceRecord(SPP_UUID);
        } catch (IOException e) {
            Toast.makeText(this, "Impossibile creare un socket seriale con il dispositivo", Toast.LENGTH_SHORT).show();
            ConnessioneOK=2;
            return;
        }
        try {
            btSocket.connect();
            writer = new OutputStreamWriter(btSocket.getOutputStream());
        } catch (IOException e) {
            Toast.makeText(this, "Impossibile connettersi con il dispositivo", Toast.LENGTH_SHORT).show();
            ConnessioneOK=3;

            return;
        }
    }
    public void inviaBluetooth(String mexColore){

        try {
            //qui c'era writer new ecc
            writer.write(mexColore);
            writer.flush();

        } catch (IOException e) {
            Toast.makeText(this, "Impossibile inviare il messaggio al dispositivo", Toast.LENGTH_SHORT).show();
        }
    }

    public int getReduced(int n){//riduce il colore in base all'hue
        return Math.round(n*hue/255);
    }

}
