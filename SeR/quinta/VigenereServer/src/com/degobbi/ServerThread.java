package com.degobbi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerThread implements Runnable {
    Socket connessione;
    String ipConnessione;
    int porta;
    ServerThread(Socket connessione){
        this.connessione=connessione;
        ipConnessione=connessione.getInetAddress().getHostAddress();
        this.porta=connessione.getPort();
    }
    @Override
    public void run() {
        ObjectInputStream inputStream = null;
        ObjectOutputStream outputStream = null;
        try {//inizializzo gli streams
            outputStream = new ObjectOutputStream(connessione.getOutputStream());
            outputStream.flush();
            inputStream = new ObjectInputStream(connessione.getInputStream());

            //System.out.println("in e out creati");
        }catch (Exception e){
            System.out.println("Errore nella creazione degli stream");
        }
        try{//messaggio iniziale
        outputStream.writeObject("Ciao sei entrato nel server, scrivi un messaggio:");
        outputStream.flush();
        }catch (Exception e){
            System.out.println("Errore messaggio iniziale");
        }
        boolean connessioneAttiva=true;
        while(connessioneAttiva) {//main
            try {
                String ricevuto= (String) inputStream.readObject();
                System.out.println("Messaggio da "+ipConnessione+":"+porta+"-->"+ricevuto);
                outputStream.writeObject(Vigenere.decripto(ricevuto));

            } catch (Exception e) {
                //System.out.println("errore nell'invio del messaggio");
                try {//chiudo il socket
                    connessione.close();
                    connessioneAttiva=false;
                    System.out.println("chiusa connessione con "+ ipConnessione+":"+porta);
                } catch (IOException e2) {
                    System.out.println("close fallito");
                }
            }
        }

       /* try {//chiudo il socket
            connessione.close();
        } catch (IOException e) {
            System.out.println("close fallito");
        }*/

    }
}
