package com.degobbi;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {

    public static void main(String[] args) {
        ServerSocket ss = null;

        try {
            ss=new ServerSocket(3000);
        } catch (IOException e) {
            System.out.println("Errore nella creazione del ss");
        }

        while(true){
            try{
                Socket s = ss.accept(); // pongo il server in attesa di connessioni
                ServerThread serverThread = new ServerThread(s);
                Thread thread = new Thread(serverThread);
                thread.start();
                System.out.println("nuovo thread");



            } catch(Exception e){
                System.out.println("errore nella accettazione della connessione");
            }
        }
    }
}
