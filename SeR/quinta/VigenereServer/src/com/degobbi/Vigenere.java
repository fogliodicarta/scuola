package com.degobbi;

public class Vigenere {
    private final static String alfabeto="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,.!?;:";
    private final static String worm="VERME";

    public static String decripto(String criptata){
        char[] decifrata=criptata.toCharArray();
        for (int i=0;i<decifrata.length;i++){
            if (decifrata[i]!=' ') {
                int indiceSottr = alfabeto.length() * 2 + alfabeto.indexOf(decifrata[i]) - alfabeto.indexOf(worm.charAt(i % worm.length()));
                indiceSottr = indiceSottr % alfabeto.length();
                decifrata[i] = alfabeto.charAt(indiceSottr);
            }
        }
        return new String(decifrata);
    }
}
