#include "crc.c"
#include "VirtualWire.h"
#define RX_PIN 12
#define TX_PIN 13
char io[] = "A09\0";

void setup() {
  Serial.begin(9600);
  vw_set_rx_pin(RX_PIN);
  vw_set_tx_pin(TX_PIN);
  vw_setup(2000);
}

void loop() {
  uint8_t maxlength = VW_MAX_MESSAGE_LEN;
  uint8_t ricevuto[maxlength]; //contiene il messaggio rx
  String msgtx;                //stringa da inviare non ancora traformata in char*
  String tast;                 //input della tastiera senza il destinatario
  if (Serial.available() > 0) {//input da tastiera inizia le operazioni necessarie alla trasmissione
    String input = Serial.readString();//input del monitor seriale con il destinatario,trattino e messaggio
    tast = input.substring(4);
    String strdest = input.substring(0,3);//input del monitor seriale con solo destinatario
    int tastlen = tast.length();      //lunghezza del messaggio tast in int
    char lsend[4];                    //lunghezza del messaggio tast in char*
    sprintf(lsend, "%03d", 8 + tastlen); //converto la lunghezza da int a char*
    lsend[3] = '\0';
    msgtx = String("A09") + strdest + String(lsend) + tast; //String da inviare senza crc
    unsigned char msgtxchar[VW_MAX_MESSAGE_LEN];      //unsigned char[] da inviare 
    msgtx.toCharArray(msgtxchar, tastlen + 13);       //converto da String a char*
    msgtxchar[tastlen + 8] = '\0';
    long crclong = crcSlow(msgtxchar, tastlen + 8);//calcolo il crc da inviare
    unsigned char crcchar[4];                      //crc in char*
    sprintf(crcchar, "%04X", crclong);             //converto il crc in char* esadecimale maiuscolo
    strcat(msgtxchar, crcchar);                   //concateno al char[] da inviare il crc 
    Serial.print("Io: ");
    Serial.print(tast);                         //mostro sul monitor seriale il messaggio da me inviato preceduto da "Io:"
    msgtxchar[tastlen + 13] = '\0';
    bool ackArrivato = false; 
    for (int i = 0; i < 6 && !ackArrivato; i++) {//se non arriva l'ack il messaggio viene inviato al massimo 5 volte
      uint8_t ackarrivo[maxlength];              //contiene l'ack arrivato
      bool ric;
      vw_send((char*)msgtxchar, strlen(msgtxchar));//invio ilmessaggio
      vw_wait_tx();
      vw_rx_start();                               //ricevo ack
      ric = vw_get_message(ackarrivo, &maxlength);           
      vw_wait_rx_max(random(2000,3000));
      if (ric) {                       //controllo se il messaggio arrivato è valido e continuo analizzando l'ack
        dest[0] = ackarrivo[3];        //destinatario dell'ack ricevuto
        dest[1] = ackarrivo[4];
        dest[2] = ackarrivo[5];
        dest[3] = '\0';
        if (strncmp(dest, io, 3) == 0) {  //controllo se il destinatario sono io
          long crcacklong = crcSlow((char*)ackarrivo, 9);     
          char crcackcalc[5];
          sprintf(crcackcalc, "%04X\0", crcacklong); // calcolo il crc dell'ack e lo converto in esadecimale maiuscolo
          char crcackric[4];
          crcackric[0] = ackarrivo[9];
          crcackric[1] = ackarrivo[10];
          crcackric[2] = ackarrivo[11];
          crcackric[3] = ackarrivo[12];
          if (strncmp(crcackric, crcackcalc, 4) == 0) { //controllo se il crc dell'ack inviato corrisponde
            ackArrivato = true;
            Serial.println("ack arrivato: destinatario e crc giusti");//se corrisponde lo segnalo nel monitor seriale
          }
        }
      }
    }
  }
  vw_rx_start();                                //inizio operazioni di ricezione
  bool isReceived = vw_get_message(ricevuto, &maxlength);//ricevo il messaggio
  vw_wait_rx_max(2000);
  if (isReceived) {                    //controllo se è stato ricevuto un messaggio
    char dest[3];                      //char[] necessario a controllare se il messaggio è indirizzato a me
    dest[0] = ricevuto[3];  
    dest[1] = ricevuto[4];
    dest[2] = ricevuto[5];
    char lricevutochar[4];             //lunghezza ricevuta nel messaggio in char*                             
    lricevutochar[0] = ricevuto[6];
    lricevutochar[1] = ricevuto[7];
    lricevutochar[2] = ricevuto[8];
    lricevutochar[3] = '\0';
    int lric = atoi(lricevutochar);    //lunghezza ricevuta convertita in int
    ricevuto[lric + 4] = '\0';
    if (strncmp(dest, io, 3) == 0) {   //controllo se sono il destinatario
      long crcriclong = crcSlow((char*)ricevuto, lric);//calcolo crc del messaggio ricevuto
      char crcriccalc[5];
      sprintf(crcriccalc, "%04X\0", crcriclong);   //converto il crc in char* esadecimale maiuscolo
      char crcricric[4];
      crcricric[0] = ricevuto[lric];
      crcricric[1] = ricevuto[lric + 1];
      crcricric[2] = ricevuto[lric + 2];
      crcricric[3] = ricevuto[lric + 3];
      if (strncmp(crcricric, crcriccalc, 4) == 0) {//controllo la validità del crc
        char mitt[4];                               //contiene il mittente del messaggio ricevuto
        strncpy(mitt, (char*)ricevuto, 3);
        mitt[3] = '\0';
        Serial.print(mitt);         //mostro nel monitor seriale chi mi ha inviato il messaggio
        Serial.print(": ");
        char mex[lric-8];           //contiene il messaggio senza imbustamenti
        for(int i=0;i<lric-9;i++){
          mex[i]=(char)ricevuto[i+9];
          }
          mex[lric-9]='\0';
          Serial.println(mex);    //mostro il messaggio sul monitor seriale
        char acksend[13] = "A09"; //contiene l'ack da inviare
        strcat(acksend, mitt);
        strcat(acksend, "ACK\0");
        long crclongsend = crcSlow(acksend, 9); //calcolo crc dell'ack
        unsigned char crccharsend[4];
        sprintf(crccharsend, "%04X", crclongsend);//converto crc in char* esadecimale maiuscolo
        strcat(acksend, crccharsend);            //concateno il crc al messaggio di ack
        vw_send((uint8_t*)acksend, strlen(acksend)); //invio l'ack
        vw_wait_tx();
      }
    }
  }
}
