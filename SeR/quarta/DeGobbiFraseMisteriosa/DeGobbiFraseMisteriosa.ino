#include "crc.c"
#include <VirtualWire.h>
#define RX_PIN 12
char misteriosa[200];
void setup() {
  Serial.begin(9600);
  vw_set_rx_pin(RX_PIN);
  vw_setup(2000);
  for (int j = 0; j < 200; j++) {
    misteriosa[j] = ' ';
  }
}

void loop() {
  uint8_t ricevuta[VW_MAX_MESSAGE_LEN];
  uint8_t msgLen = VW_MAX_MESSAGE_LEN;
  vw_rx_start();
  vw_wait_rx();
  bool corretto;
  do {
    corretto = vw_get_message(ricevuta, &msgLen);
  } while (!corretto);
  int lenTot = strlen(ricevuta);
  ricevuta[13] = '\0';
  unsigned char posLettera[5];
  posLettera[0] = ricevuta[0];
  posLettera[1] = ricevuta[1];
  posLettera[2] = ricevuta[2];
  posLettera[3] = ricevuta[3];
  posLettera[4] = '\0';
  if (misteriosa[atoi(posLettera)] != ricevuta[8]) {
    unsigned char info[lenTot - 4];
    for (int i = 0; i < lenTot - 4; i++) {
      info[i] = ricevuta[i];
    }
    info[lenTot - 4] = '\0';
    unsigned char crc[5];
    for (int i = 0; i < 4; i++) {
      crc[i] = ricevuta[i + lenTot - 4];
    }
    crc[4] = '\0';

    unsigned char crcStr[5];
    ltoa(crcSlow(info, lenTot - 4), crcStr, 16);
    crcStr[0] = toupper(crcStr[0]);          // converte le cifre esadecimali in maiuscolo
    crcStr[1] = toupper(crcStr[1]);
    crcStr[2] = toupper(crcStr[2]);
    crcStr[3] = toupper(crcStr[3]);
    crcStr[4] = '\0';
    char lungParola[5];
    lungParola[0] = info[4];
    lungParola[1] = info[5];
    lungParola[2] = info[6];
    lungParola[3] = info[7];
    lungParola[4] = '\0';
    int k = atoi(lungParola) + 1;
    if (strncmp(crc, crcStr, 4) == 0) { //viene eseguito solo se il crc inviato è uguale a quello calcolato
      misteriosa[k] = '\0';
      misteriosa[atoi(posLettera)] = (char)info[8];
    }
  }
  Serial.println(misteriosa);




}
