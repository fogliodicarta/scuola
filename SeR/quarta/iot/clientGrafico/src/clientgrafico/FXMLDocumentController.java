package clientgrafico;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;

/**
 *
 * @author Matteo
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button importa;

    @FXML
    private Button salva;

    @FXML
    private Button elimina;

    @FXML
    NumberAxis yAxis = new NumberAxis();

    @FXML
    NumberAxis xAxis = new NumberAxis();

    @FXML
    ScatterChart<Number, Number> scatterChart = new ScatterChart(xAxis, yAxis);

    @FXML
    private TextArea testo;
    /* @FXML
     private Label label;
     */

    @FXML
    private void bottoneImporta(ActionEvent event) throws Exception {
        testo.setText("");
        scatterChart.getData().clear();
        String ricevuta;
        ricevuta = null;
        do {
            System.out.println("Richiedendo file...");
            try {
                ClientDatagram.invia("getfile");
            } catch (Exception ex) {

            }
            ricevuta = ClientDatagram.ricevi();

        } while (ricevuta == null);
        String[] splittato = ricevuta.split(",", 0);
        int len = 30;
        if (splittato.length >= 1) {
            XYChart.Series series = new XYChart.Series();
            int[] lux = new int[splittato.length];
            for (int i = 0; i < splittato.length; i++) {
                lux[i] = Integer.parseInt(splittato[i].substring(24, 28));
                series.getData().add(new XYChart.Data(i, lux[i]));
                testo.appendText(splittato[i].substring(4,14)+' '+splittato[i].substring(15,23)+' '+ Integer.toString(lux[i])+" Lux\n");
            }
            scatterChart.getData().addAll(series);
        }
        
    }

    @FXML
    private void bottoneSalva(ActionEvent event) throws Exception {
        ClientDatagram.invia("save");
    }

    @FXML
    private void bottoneElimina(ActionEvent event) throws Exception {
        ClientDatagram.invia("delete");
        testo.setText("");
        scatterChart.getData().clear();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ClientDatagram.inizia();
            testo.setEditable(false);
        } catch (Exception ex) {

        }
    }
}
