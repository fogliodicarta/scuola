package clientgrafico;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class ClientDatagram {

    public static String ipServer = "192.168.8.27";

    public static DatagramSocket datagramSocketRicezione;
    
    public static void inizia() throws Exception{
        datagramSocketRicezione=new DatagramSocket(8000);
        datagramSocketRicezione.setSoTimeout(5000);
    }
    
    public static void invia(String messaggio) throws Exception {
        System.out.println("Sto inviando: " + messaggio);
        byte[] buffer = messaggio.getBytes();
        DatagramSocket datagramSocketInvio;
        datagramSocketInvio = new DatagramSocket();
        InetAddress receiverAddress;
        receiverAddress = InetAddress.getByName(ipServer);
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, receiverAddress, 8000);
        datagramSocketInvio.send(packet);
    }

    public static String ricevi()throws Exception {
        String msg = null;
        try {
            byte[] buffer = new byte[20000];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            //System.out.println("Inizio a ricevere");
            datagramSocketRicezione.receive(packet);
            msg = new String(packet.getData(), 0, packet.getLength());
            //System.out.println(msg);

        } catch (IOException e) {
            System.out.println("Timeout Raggiunto");
        }
        return msg;
    }

}
