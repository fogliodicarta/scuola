#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include "TimeLib.h"
int pinLuce = 14 ;
double valoriLuce = 0;
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED //indirizzo mac della board
};
IPAddress server(192,168,1,108);//indirizzo IP del server
IPAddress ip(192, 168, 1, 180);//indirizzo IP della board
IPAddress myDns(192, 168, 100, 51);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
unsigned int localPort = 8000; //inizializzazione della porta in ascolto
EthernetUDP Udp;
void setup() {
  Ethernet.init(10);
  Serial.begin(9600);
  while (!Serial) {;
  }
  Ethernet.begin(mac, ip, myDns, gateway, subnet);//init del modulo Ethernet
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP()); 
  Udp.begin(localPort);//init dell'oggetto EthernetUdp
  boolean nonRicevuta = true;
  char packetBuffer [20] ;
  packetBuffer[19] = '\0'; 
  do{
    Udp.beginPacket(server, 8000);//
    Udp.write("dateRequest");
    Serial.println("ciao");
    Udp.endPacket();
    int packetSize = Udp.parsePacket();
    nonRicevuta = packetSize==0;
  }while(nonRicevuta); 
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(packetBuffer);
    String dataArrivata(packetBuffer);
    Serial.println("matteo");
    Serial.println(dataArrivata.substring(0,2).toInt());
    setTime(dataArrivata.substring(11,13).toInt(),dataArrivata.substring(14,16).toInt(),dataArrivata.substring(17,19).toInt(),dataArrivata.substring(0,2).toInt(),dataArrivata.substring(3,5).toInt(),dataArrivata.substring(6,10).toInt());
}
void loop() {
  for(int i=0;i<15;i++){
  valoriLuce = analogRead(pinLuce);
  Serial.print("Valori Luce : ");
  Serial.println(valoriLuce);
  char dataInvio[20];
  sprintf(dataInvio,"%02d/%02d/%04d_%02d:%02d:%02d ",day(),month(),year(),hour(),minute(),second());
  Serial.println(day());
  char informazioni [33] ;
  sprintf(informazioni,"%s%s%04d","lux ",dataInvio,(int)valoriLuce);
  Udp.beginPacket(server, 8000);
  Udp.write(informazioni);
  Udp.endPacket();
  delay(500);
  }
  Udp.beginPacket(server, 8000);
  Udp.write("save");
  Udp.endPacket();
}
