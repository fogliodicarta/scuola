/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package provajavanet1;

import java.io.IOException;
import java.net.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Server {

    public static InetAddress lastIp;

    public static DatagramSocket datagramSocketRicezione;

    public static File dati;
    public static PrintWriter printWriter;

    public static String dataCorrente() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy_HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    public static void invia(String messaggio) throws Exception {
        System.out.println("Sto inviando: " + messaggio);
        byte[] buffer = messaggio.getBytes();
        DatagramSocket datagramSocketInvio;
        datagramSocketInvio = new DatagramSocket();
        InetAddress receiverAddress;
        receiverAddress = lastIp;
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length, receiverAddress, 8000);
        datagramSocketInvio.send(packet);
    }

    public static String ricevi() {
        String msg = null;
        try {

            byte[] buffer = new byte[30];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            //System.out.println("Inizio a ricevere");
            datagramSocketRicezione.receive(packet);
            lastIp = packet.getAddress();
            System.out.println(lastIp);
            msg = new String(packet.getData(), 0, packet.getLength());
            //System.out.println(msg);

        } catch (IOException e) {
            System.out.println(e);
        }
        return msg;
    }

    public static void main(String[] args) throws Exception {
        datagramSocketRicezione = new DatagramSocket(8000);
        dati = new File("dati.txt");
        printWriter = new PrintWriter(new FileWriter(dati, true));
        while (true) {
            String ricevuta = ricevi();
            System.out.println("Ricevuto messaggio: " + ricevuta);
            if (ricevuta.equals("dateRequest")) {                 //Richiesta della data  
                invia(dataCorrente());
            } else if (ricevuta.substring(0, 3).equals("lux")) {  //Dati del sensore
                printWriter.print(ricevuta + ",");
            } else if (ricevuta.equals("save")) {                  //Richiesta di salvare i dati su file
                printWriter.close();
                printWriter = new PrintWriter(new FileWriter(dati, true));
            } else if (ricevuta.equals("delete")) {                  //Richiesta di eliminare i dati dal file
                printWriter.close();
                printWriter = new PrintWriter(new FileWriter(dati, false));
            } else if (ricevuta.equals("getfile")) {                 //Richiesta del file dati da parte del visualizzatore
                printWriter.close();
                String contenuto = new String(Files.readAllBytes(Paths.get("dati.txt")));
                System.out.println("File " + contenuto);
                invia(contenuto);
                printWriter = new PrintWriter(new FileWriter(dati, true));
            }
        }
    }
}
