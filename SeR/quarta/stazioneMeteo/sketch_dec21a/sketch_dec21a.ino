#include "TimeLib.h"
#include "VirtualWire.h"
#include <LiquidCrystal.h>

#define RX_PIN 11
#define INFO_TYPE 8


String ora;
String luce;
LiquidCrystal lcd(13, 10, 5, 4, 3, 2);

uint8_t maxLength = VW_MAX_MESSAGE_LEN;
uint8_t messaggio[VW_MAX_MESSAGE_LEN];

void setup() {
  lcd.begin(16, 2);
  vw_set_rx_pin(RX_PIN);
  vw_setup(3000);
  Serial.begin(9600);
}

String hour(char* s) {
  char orario[9];
  int i = 9;
  int act = 0;
  while (i < 15) {
    orario[act++] = s[i++];
    orario[act++] = s[i++];
    orario[act++] = ':';
  }
  orario[act-1]='\0';
  String output(orario);
  return output;
}

String lux(char* s){
    char luce[5];
    int i = 9;
    int act =0;
    for(;i<13;i++){
        luce[act++]=s[i];
    }
    luce[4]='\0';
    String output(luce);
    return output;
}

void lcdPrint(String ora, String luce){
  
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Ora "+ora);
    lcd.setCursor(0, 1);
    lcd.print("Luce "+luce+" Lux");
  }

void loop() {
    int len=0;
    uint8_t maxLength = VW_MAX_MESSAGE_LEN;
    uint8_t messaggio[VW_MAX_MESSAGE_LEN];
    char mex[VW_MAX_MESSAGE_LEN];
    vw_rx_start();
    vw_get_message(messaggio,&maxLength);
    vw_wait_rx();
    for(int i = 0;i<VW_MAX_MESSAGE_LEN ;i++){
        mex[i]=((char)messaggio[i]);
        len++;
      } 
      if(mex[INFO_TYPE]=='O'){
        ora=hour(mex);
        Serial.println(ora);  
      }else if(mex[INFO_TYPE]=='L'){
        luce=lux(mex);  
        Serial.println(luce);
      }
    lcdPrint(ora,luce);
    
}
