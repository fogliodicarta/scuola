<!-- background: #333333 -->

# Documentazione progetto in Arduino `Stazione Metereologica`
## Leonardo Luigi Pepe & Matteo De Gobbi
### ITIS C. Zuccante A.S. 2018/2019 Classe 4^IB

#### Idea del progetto
- In questo progetto viene proposta la simulazione di una stazione metereologica che invia i dati climatici in broadcast
- La simulazione prevede la comunicazione di due dispositivi: `trasmettitore` e `ricevitore`
- Il `trasmettitore` invierà in broadcast l'`ora` e la `luce` (misurata in _Lux_)
- Il `ricevitore` riceverà in input le informazioni trasmesse da un `trasmettitore`
- Le informazioni ottenute dal trasmettiore verranno mostrate sia su un `Display LCD` a 16 colonne e 2 righe e sia trasmesse tramite una comunicazione seriale

<hr>

# Struttura del progetto

## Hardware

- Arduino Uno
- `Display LCD` a 16 colonne e 2 righe
- Fotoresistore
- Modulo ricevitore `XD-RF-5V`
- Modulo trasmettitore `FS1000A`

_Tutto lo schema elettrico, quindi resistenze, cavi e breadboard sono all'interno del_ [progetto fritzing](https://drive.google.com/file/d/1k2FlQH3WZHb7blHOYXE_MrzkvyVw7QQk/view?usp=sharing)

### Schema elettrico trasmettitore
![](trasmettitore.png)

### Schema elettrico ricevitore
![](ricevitore.png)
## Software

- Per la programmazione dell'Arduino è stato utilizzato l'[IDE gratuito](https://www.arduino.cc/en/main/software) distribuito dall'azienda ufficiale

>L'ambiente di sviluppo integrato di Arduino è fornito di una libreria software C/C++, chiamata "Wiring" (dall'omonimo progetto Wiring): la disponibilità della libreria rende molto più semplice implementare via software le comuni operazioni di input/output. I programmi di Arduino sono scritti in linguaggio derivato dal C/C++, ma all'utilizzatore, per poter creare un file eseguibile, si richiede solo di definire due funzioni: `void setup()` e `void loop()`
[fonte](https://it.wikipedia.org/wiki/Arduino_(software)

- Sono state implementate le librerie `Time` e `VirtualWire`
- `Time` permette di impostare un orario all'Arduino e di effettuare varie operazioni su di esso
- `VirtualWire` rende possibile la comunicazione tramite i due moduli di `trasmissione` e `ricezione`

_Si allegano le documentazioni delle relative librerie_

### Software trasmettitore
- codice 1
```
codice codice codice
```
- codice 2

### Software ricevitore

>Nella seguente descrizione non verranno proposte tutte le righe di codice scritte ma solo quelle necessarie per comprendere il funzionamento del progetto. Metodi di manipolazione di stringhe o codice già proposto nella documentazione delle librerie sarà omesso

##### Setting delle MACRO

```
#define RX_PIN 11
#define INFO_TYPE 8
```
>La direttiva `#define` serve a definire una `MACRO`, ovvero un simbolo.
Il preprocessore legge la definizione della `MACRO` e, ogni volta che ne incontra il nome all'interno del file sorgente SOSTITUISCE al simbolo il corrispondente valore, SENZA verificare la correttezza sintattica dell'espressione risultante.

La scelta di usare le MACRO risulta migliore perché, essendo un simbolo che il preprocessore sostituisce, sono più leggere da immagazzinare in memoria.

- `RX_PIN` indica il pin usato nell'arduino per effettuare la ricezione
- `INFO_TYPE` indica la posizione del carattere, all'interno del messaggio ricevuto, che indica se il messaggio trasmesso è l'ora attuale o la luce misurata

##### Dichiarazione Oggetti

```
String ora;
String luce;
LiquidCrystal lcd(13, 10, 5, 4, 3, 2);
```
- `ora` è una stringa contenente l'orario
- `luce` è una stringa contenente la luce in _Lux_
- `lcd` è un oggetto della classe `LiquidCrystal` che permette l'utilizzo di uno schermo lcd. Il costruttore richiede di inserire i pin utilizzati per la comunicazione del dispositivo (viene usato il modulo `I2C`)

##### Dichiarazione Unsigned Integer of length 8 bits (`uint8_t`)

```
uint8_t maxLength = VW_MAX_MESSAGE_LEN;
uint8_t messaggio[VW_MAX_MESSAGE_LEN];
```
>È molto utile usare `uint8_t` quando si lavora sulla computazione di bit, o in un range specifico di valori perché non è necessario "individuare" la lunghezza di una qualsiasi variabile tipizzata, si utilizza effettivamente quello di cui si ha bisogno. In questo caso nella variabile possono essere contenuti valori da 0 a 255 e si conosce quindi già la lunghezza della locazione di memoria. Questo tipo di variabile, perlopiù, occupa meno memoria rispetto a qualisiasi altra variabile tipizzata come `int` o `long`

- `maxLength` contiene la lunghezza massima di un messaggio con un valore assegnatogli da una MACRO della libreria `VirtualWire`
- `messaggio` contiene il messsaggio con una lunghezza massima parti a `VW_MAX_MESSAGE_LEN`

##### Setup

```
void setup() {
  lcd.begin(16, 2);
  vw_set_rx_pin(RX_PIN);
  vw_setup(3000);
  Serial.begin(9600);
}
```
- `lcd.begin(16,2)` definisce il numero di colonne (16) e righe (2) dell'lcd
- `vw_set_rx_pin` imposta il pin per la ricezione
- `vw_setup` imposta il baud rate della comunicazione
- `Serial.begin(9600)` imposta un baud rate di `9600 ms` per la comunicazione seriale

##### Metodi implementati
- `String hour(char* s)` restituisce l'ora che viene ricevuta dal trasmettitore il quale invierà il messaggio seguendo il protocollo `#inizio#Ohhmmss#fine#` (hhmmss indica l'orario in ora:minuti:secondi)
- `String lux(char *s)` restituisce la luce in _Lux_ che viene ricevuta dal trasmettitore il quale invierà il messaggio seguendo il protocollo `#inizio#Lxxxx#fine#` (xxxx indica un valore)
- `void lcdPrint(String ora, String luce)` stampa sul display lcd le informazioni riguardanti orario e luce

##### Loop

###### Copia del messaggio ricevuto

```
char mex[VW_MAX_MESSAGE_LEN];
//altro codice
//altro codice
//altro codice
for(int i = 0;i<VW_MAX_MESSAGE_LEN ;i++){
    mex[i]=((char)messaggio[i]);
 }
```

###### Valutazione del messaggio ricevuto e stampa su Display LCD

```
if(mex[INFO_TYPE]=='O'){
 ora=hour(mex);
 Serial.println(ora);  
}else if(mex[INFO_TYPE]=='L'){
 luce=lux(mex);  
 Serial.println(luce);
}
lcdPrint(ora,luce);
```
- Se il carattere che definisce il tipo di informazione è `O` allora viene aggiornata l'ora e stampata nel monitor seriale; altrimenti se il carattere che definisce il tipo di informazione è `L` allora viene aggiornato il valore della luce e poi stampato nel monitor seriale.
- Sucessivamente vengono stampati orario e luce sul Display LCD
<hr>
<footer>
  <p>Documentazione scritta da: Leonardo Luigi Pepe</p>
  <p>Progetto fritzing realizzato da: Matteo De Gobbi</p>

</footer>
