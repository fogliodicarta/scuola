#include "TimeLib.h"
#include "VirtualWire.h"

const int TX_PIN = 12;
const int LUX_PIN = 0;

void setup() {
  vw_set_tx_pin(TX_PIN);
  vw_setup(3000);
  pinMode(TX_PIN, OUTPUT);
  setTime(0, 0, 0, 1, 12, 2018);
  Serial.begin(9600);
}
void loop() {
  char messaggio[30];
  sprintf(messaggio, "%s%02d%02d%02d%s", "#inizio#O", hour(), minute(), second(), "#fine#\0");
  Serial.println(messaggio);
  vw_send((uint8_t *)messaggio, strlen(messaggio));
  vw_wait_tx();

  sprintf(messaggio, "%s%04d%s", "#inizio#T", analogRead(LUX_PIN), "#fine#\0");
  Serial.println(messaggio);
  vw_send((uint8_t *)messaggio, strlen(messaggio));
  vw_wait_tx();
  delay(1000);
}
