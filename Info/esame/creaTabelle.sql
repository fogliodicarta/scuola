CREATE TABLE `employee` (
  `idEmployee` int(11),
  `codice` char(5) UNIQUE,
  `cognome` varchar(25),
  `nome` varchar(25),
  `mail` varchar(50) UNIQUE, 
  `password` char(8),
  PRIMARY KEY (`idEmployee`)
) ENGINE=InnoDB DEFAULT charset='utf8' COLLATE 'utf8_general_ci';

CREATE TABLE `authorization` (
  `idAuthorization` int(11),
  `livello` int(11),
  `tipo` enum('finance','it','hr','legale','erp'),
   PRIMARY KEY (`idAuthorization`)
)ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE 'utf8_general_ci';

CREATE TABLE `authEmp` (
  `employeeId` int(11),
  `authorizationId` int(11),
  PRIMARY KEY (employeeId,authorizationId),
  CONSTRAINT `fkemployee` FOREIGN KEY (`employeeId`) REFERENCES `employee` (`idEmployee`),
  CONSTRAINT `fkauth` FOREIGN KEY (`authorizationId`) REFERENCES `authorization` (`idAuthorization`)
)ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE 'utf8_general_ci';

INSERT INTO `employee` VALUES (1,'00001','Giallo','Giovanni','gg',''),(2,'0000A','Rossi','Mario','rm',''),(3,'00002','Bianchi','Enrico','be',''),(4,'0000B','Verdi','Arturo','va',''),(5,'00003','Verdi','Giuseppe','vg',''),(6,'0000C','Verdi','Stefano','vs',''),(7,'00004','Grigio','Luca','gl','');

INSERT INTO `authorization` VALUES (1,1,'finance'),(2,2,'finance'),(3,3,'finance'),(4,1,'it'),(5,2,'it'),(6,1,'hr'),(7,1,'legale'),(8,1,'erp');

INSERT INTO `authEmp` VALUES (1,1),(1,8),(2,3),(2,6),(2,7),(3,5),(4,5),(4,8),(5,6),(6,6),(7,8);

SELECT nome,cognome,tipo,livello FROM employee JOIN (authEmp,authorization)ON(idEmployee=employeeId AND idAuthorization = authorizationId) ORDER BY idEmployee;

