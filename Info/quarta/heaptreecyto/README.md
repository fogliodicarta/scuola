Heap Tree in Javascript/Cytoscape.js
====
#### Sono state utilizzate le librerie:
* Cytoscape.js per la rappresentazione dei grafi.
* Sheetsu per la lettura e l'importazione dei dati del grafo da un tabella di Google Sheetsu.

##### Note sulle librerie:
* La libreria sheetsu nella versione free permette solo di leggere da un foglio google quindi non ho potuto caricare i dati del grafo e salvarli.
* La libreria cytoscape.js nell'ultima versione ha dei bug nella gestione delle animazioni e dello style dei nodi ad esempio:
    * Tra un cambio di colori dei nodi ed un altro bisogna aspettare almeno 500ms tramite `setTimeout` o il programma rimarrà bloccato sullo stesso colore per il resto dell'esecuzione.
    * In alcuni casi le animazioni vengono eseguite appena vengono dichiarate, ciò non è specificato nella documentazione di Cytoscape.js e, volendo eseguire le animazioni una per volta ho creato uno stack che contiene i dati necessari alla creazione dell'animazione e quando ne ho bisogno chiamo una funzione che costruisce le animazioni in base ai dati nello stack.
* La documentazione di Cytoscape.js spesso mostra esempi inadatti o proprio sbagliati e in alcuni casi ho dovuto provare per tentativi in quanto non erano specificati alcuni parametri necessari alle funzioni.
_____
Scrittura del programma
-----------
Non conoscendo bene la libreria ho deciso di procedere a step:
1. Inizialmente creando un programma che aggiungesse nodi e edge in base agli input dati tramite un `<input type="text">` di HTML e due `<button type="button"></button>`
2. Poi dovendo rappresentare un Heap tree ho cercato il `layout` più adatto tra quelli disponibili in Cytoscape.js ho trovato che il `layout breadthfirst` con le proprietà settate con grid attiva e come  root sempre il nodo 0 riesce a rappresentare bene un albero
3. Dopo ho aggiunto un `<button>` che esegue il maxheap sull'albero ed uno per rimuovere i nodi dall'albero
4. Per mostrare meglio il funzionamento del `buildMaxHeap` ho diviso l'animazione in singoli passaggi che l'utente può vedere uno alla volta tramite il pulsante `next` o insieme tramite il pulsante `disegna`
5. Ho aggiunto le animazioni anche per la rimozione dei nodi e per l'aggiunta
6. Aggiunta la possibilità di importare grafi da tabelle di Fogli Google tramite il `<button> Importa </button>`
7. Aggiunto il pulsante `zoom` che svolge la stessa azione di `disegna` ma con una animazione diversa simile a quella dell'aggiunta di un conoscendo
8. Aggiunta una tabella tramite `<iframe>` che mostra lo stato corrente della tabella google sheets da cui si importa
9. Aggiunta una leva che permette di selezionare la modalità di funzionamento del programma (Arancione=Maxheap, Verde=Minheap)
_________________________________________
Codice
---
Le funzioni: `swap`, `parent`, `left`, `right`, `isLeaf`, `insertx`, `buildMinHeap`, `buildMaxHeap`, `minHeapify`, `maxHeapify`,
# AGGIUNGI SORT
servono alla gestione dell'heap tree e non hanno avuto bisogno di modifiche strutturali rispetto alla versione Java del programma

La funzione collegata al pulsante `importa` e la funzione `datiArrivati` servono per importare dati dal foglio google tramite la libreria sheetsu
#### Animazioni
Per alcune animazioni è stata usata la `codaAnimazioni` necessaria per vedere l'animazione a step, nella coda non viene salvata la vera e propria animazione ma piuttosto i dati necessari per lanciarla

Le funzioni `disegnatutto` e `zoomAndDisegna` sono due funzioni simili per disegnare l'albero dotate di animazioni diverse usate in casi diversi

La funzione `edgeadder` è molto importante, e tutti i metodi di disegno dell'albero si basano su questa funzione che crea delle animazioni ricorsivamente collegando i vari nodi dell'albero

La funzione `animazioneScambio` serve per scambiare di posizione due nodi mentre l'edge che li collega sfuma, per fare questa animazione è stato necessario fare una deep copy dell'oggetto `position` tramite `JSON.stringify` altrimenti i due nodi cercando di andare alla posizione dell'altro usando un reference copiato tramite shallow copy finivano con l'incontrarsi a metà strada uno dall'altro e rimanere sovrapposti a metà del loro percorso

La funzione `hoistNode` crea un'animazione ricorsiva che solleva i nodi sottostanti ad uno appena rimosso in modo da riempire gli spazi mancanti nell'albero
#### Event listeners
Sono presenti vari event listeners dichiarati nel `window.onload` necessari per il funzionamento di pulsanti, input e la leva di selezione modalità
