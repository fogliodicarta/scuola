import java.util.Iterator;
import java.util.Stack;

public class BinaryTree {//TRE STACK DIVERSI COSì SE UNO è MONA PUò ATRAVERSARE IN PIù MODI CONTEMPORANEMENTE
    Node root;
    int nNodi = 0;

    public BinaryTree(int val) {
        root = new Node(val);
        nNodi++;
    }

    public void add(int val) {
        Node current = root;
        Node genitore = null;
        while (current != null) {
            genitore = current;
            current = val < current.dato ? current.left : current.right;
        }
        current = new Node(val);
        current.parent = genitore;
        if (current.dato < genitore.dato) {
            genitore.left = current;
        } else {
            genitore.right = current;
        }
        nNodi++;
    }

    public void stampaPre() {
        PreOrder a = new PreOrder();
        while (a.hasNext()) {
            System.out.print(a.next().dato + "; ");
        }
        System.out.println();
        InOrder b = new InOrder();
        while (b.hasNext()) {
            System.out.print(b.next().dato + "; ");
        }
        System.out.println();
        PostOrder c = new PostOrder();
        while (c.hasNext()) {
            System.out.print(c.next().dato + "; ");
        }
    }

    private class PreOrder implements Iterator<Node> {
        Node current;
        Stack<Node> s;

        PreOrder() {
            s = new Stack();
            s.push(root);
        }

        @Override
        public boolean hasNext() {
            return !s.empty();
        }

        @Override
        public Node next() {
            current = s.pop();
            if (current.right != null) {
                s.push(current.right);
            }
            if (current.left != null) {
                s.push(current.left);
            }
            return current;
        }
    }

    private class InOrder implements Iterator<Node> {
        Node current;
        Stack<Node> s;

        InOrder() {
            s = new Stack();
            current = root;
        }

        @Override
        public boolean hasNext() {
            return !s.empty() || current != null;
        }

        @Override
        public Node next() {
            while (current != null) {
                s.push(current);
                current = current.left;
            }
            current = s.pop();
            Node ret = current;
            current = current.right;
            return ret;
        }
    }

    private class PostOrder implements Iterator<Node> {
        Node current;
        Stack<Node> s;

        PostOrder() {
            Stack<Node> s2 = new Stack<>();
            s = new Stack<>();
            s2.push(root);
            while (!s2.empty()) {
                current = s2.pop();
                s.push(current);
                if (current.left != null) {
                    s2.push(current.left);
                }
                if (current.right != null) {
                    s2.push(current.right);
                }
            }
        }

        @Override
        public boolean hasNext() {
            return !s.empty();
        }

        @Override
        public Node next() {
            return s.pop();
        }
    }

    static class Node {
        int dato;
        Node left;
        Node right;
        Node parent;

        public Node(int dato) {
            this.dato = dato;
            parent = null;
            left = null;
            right = null;
        }
    }


}
