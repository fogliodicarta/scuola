BiLista
===
Ho deciso di creare la versione con la matrice perché l'anno scorso avevo già fatto in c una lista utilizzando una struct.

La versione con la matrice presenta dei vantaggi e degli svantaggi:
--
 1. Quando si cancella un nodo rimangono delle locazioni vuote che possono essere riempite nuovamente, ho risolto questo problema facendo in modo che al momento dell'inserimento di un nuovo nodo il programma controllasse dove fosse la prima locazione libera nella matrice
 2. Non ci sono problemi di NullPointer in quanto non ci sono reference, vengono usati int per "puntare" al nodo precedente o successivo nella lista
 3. Va specificata la lunghezza della matrice all'inizio, avrei potuto stabilire una lunghezza costante in modo static ma la lista allora avrebbe avuto un limitazione fastidiosa
 4. Maggiore facilità nell'implementare l'heapsort
 
Note sul programma
---
* La matrice è di 3xn con n=numero di nodi, avevo pensato di aggiungere una 4 linea che contenesse la posizione del nodo nella lista o una che indicasse se la locazione della matrice è occupata, ma non è servito perché ho utilizzato:
   * il valore -1 per indicare un valore null (il previous del primo nodo e il next dell'ultimo)
   * il valore -2 dentro previous per indicare che la locazione è libera e può essere utilizata per salvarci un nodo
* All'interno della lista viene salvato il valore inizio (primo nodo), non c'è stato bisogno di salvare il valore dell'ultimo nodo
* Il metodo `search` restituisce tutti i nodi che contengono ilvalore ricercato, non solo il primo
* Sono presenti dei metodi privati utilizzati per implementare altri metodi all'interno di `BiLista` e `DisegnaBiLista`
Metodi più importanti
--
* il metodo `append` aggiunge un nodo alla fine, `prepend` aggiunge un nodo all'inizio e `insert` aggiunge un nodo data la posizione, ho creato questi 3 metodi che apparentemente fanno la stessa cosa in modo che se non si vogliono usare gli indici usando una `BiLista` sia possibile farlo, in ogni caso si possono aggiungere nodi alla fine o all'inizio anche utilizzando `insert`
* il metodo `delete` elimina un nodo data la posizione e rende disponibile per la sovrascrizione la locazione della matrice dove era presente il nodo
* i metodi `toString` e `print` per visualizzare la lista su terminale
* i metodi `ArrToList` e `ListToArr` convertono rispettivamente da array di interi a `BiLista` e da `BiLista` a array di interi
* il metodo `elementAt` restituisce il valore di un nodo data la sua posizione
* il metodo `change` scambia la posizione di due nodi
* il metodo `search` che restituisce un `ArrayList` contenente le posizioni dei nodi con il valore ricercato nella lista
* il metodo `rovescia` inverte l'ordine della lista
* il metodo `sort` utilizza la modalità di heapsort per ordinare i nodi nella lista
* il metodo `disegna` che disegna a video con JavaFx la lista

![](https://i.imgur.com/Zp2yW1t.png)*BiLista mostrata a video con JavaFx*

Il disegno della lista si adatta al numero di nodi e la grandezza del font di ogni numero disenato dipende da quante cifre ha quel numero, ad esempio 8 avrà un font più grande rispetto a 100

