package com.company;

public class Main {

    public static void main(String[] args) {
        BiLista list1 = new BiLista(50);
        list1.insert(1,0);//rimanda a append
        list1.insert(2,1);
        list1.insert(3,2);//inserisco tre valori
        list1.append(4);//provo append
        list1.print();
        list1.delete(2);//provo delete
        list1.prepend(5);//provo prepend
        list1.print();
        list1.insert(6,0);//rimanda a prepend
        list1.change(0,4);//provo swap
        list1.print();
        list1.rovescia();//provo rovescia
        list1.print();
        System.out.println(list1);//provo toString
        System.out.println(list1.elementAt(1));//provo elementAt

        BiLista list2 = new BiLista(100,new int[]{8,10,200,30,400,5,60,700,8,9,12,342,31,23,32,77});
        System.out.println(list2.search(8));
        list2.sort();
        list2.append(42);
        list2.disegna(args);
    }
}
