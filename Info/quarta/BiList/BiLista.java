package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class BiLista {
    int[][] matr;
    final private int PREV = 0;
    final private int KEY = 1;
    final private int NEXT = 2;
    private int nNodi;
    private int inizio;

    BiLista(int lung) {
        matr = new int[3][lung];
        Arrays.fill(matr[PREV], -2);
    }

    BiLista(int lung, int[] arr) {
        matr = new int[3][lung];
        Arrays.fill(matr[PREV], -2);
        ArrToList(arr);
    }

    void ArrToList(int[] arr) {
        nNodi = arr.length;
        for (int i = 0; i < nNodi; i++) {
            matr[PREV][i] = i - 1;
            matr[KEY][i] = arr[i];
            matr[NEXT][i] = i + 1;
        }
        inizio = 0;
        matr[NEXT][nNodi - 1] = -1;
    }

    int[] ListToArr(){
        int[] array = new int[nNodi];
        int current = inizio;
        for(int i=0;i<nNodi;i++){
            array[i] = matr[KEY][current];
            current = matr[NEXT][current];
        }
        return array;
    }

    @Override
    public String toString() {
        String s = "";
        int current = inizio;
        while (matr[NEXT][current] > -1) {
            s = s + matr[KEY][current] + ", ";
            current = matr[NEXT][current];
        }
        s = s + matr[KEY][current];
        return s;
    }

    void print() {
        int current = inizio;
        while (matr[NEXT][current] > -1) {
            System.out.print(matr[KEY][current] + ", ");
            current = matr[NEXT][current];
        }
        System.out.println(matr[KEY][current]);
    }

    int elementAt(int pos) {//aggiungi exception
        return matr[KEY][vaiA(pos)];
    }

    void insert(int chiave, int posizione) {
        if (posizione == nNodi) {
            append(chiave);
            return;
        } else if (posizione == 0) {
            prepend(chiave);
            return;
        }
        int libero = primoLibero();
        posizione = vaiA(posizione);
        matr[PREV][libero] = matr[PREV][posizione];
        matr[NEXT][libero] = posizione;
        matr[NEXT][matr[PREV][posizione]] = libero;
        matr[PREV][posizione] = libero;
        matr[KEY][libero] = chiave;
        nNodi++;
    }

    void append(int chiave) {
        int index = vaiA(nNodi - 1);
        int libero = primoLibero();
        matr[PREV][libero] = index;
        matr[NEXT][libero] = -1;
        matr[KEY][libero] = chiave;
        matr[NEXT][index] = libero;
        nNodi++;
    }

    void prepend(int chiave) {
        int libero = primoLibero();
        matr[PREV][inizio] = libero;
        matr[PREV][libero] = -1;
        matr[NEXT][libero] = inizio;
        matr[KEY][libero] = chiave;
        inizio = libero;
        nNodi++;
    }

    void delete(int posizione) {
        posizione = vaiA(posizione);
        int prev = matr[PREV][posizione];
        int next = matr[NEXT][posizione];
        matr[NEXT][prev] = next;
        matr[PREV][next] = prev;
        nNodi--;
        matr[PREV][posizione] = -2;
    }

    void change(int pos1, int pos2) {
        pos1 = vaiA(pos1);
        pos2 = vaiA(pos2);
        int appoggio = matr[KEY][pos1];
        matr[KEY][pos1] = matr[KEY][pos2];
        matr[KEY][pos2] = appoggio;
    }

    void rovescia() {
        int current = inizio;
        while (matr[NEXT][current] > -1) {
            int appoggio = matr[NEXT][current];
            matr[NEXT][current] = matr[PREV][current];
            matr[PREV][current] = appoggio;
            current = matr[PREV][current];
        }
        int appoggio = matr[NEXT][current];
        matr[NEXT][current] = matr[PREV][current];
        matr[PREV][current] = appoggio;
        inizio = current;
    }

    void disegna(String[] args){
        DisegnaBiLista.main(args,ListToArr());
    }

    ArrayList<Integer> search(int val){// da un arraylist con tutte le posizioni in cui si trova val
        int current = inizio;
        ArrayList<Integer> ret = new ArrayList<>();
        for(int i = 0;i<nNodi;i++){
            if (val == matr[KEY][current]){
                ret.add(i);
            }
            current=matr[NEXT][current];
        }
        return ret;
    }

    void sort(){
        int[] unsorted = ListToArr();
        unsorted = heapMaxSort(unsorted);
        ArrToList(unsorted);
    }

    private int primoLibero() {//IL VALORE CHE INDICA LE LOCAZIONI LIBERE è IL PREV = -2
        int i = 0;
        for (; i < nNodi && matr[PREV][i] != -2; i++) ;
        return i;
    }

    private int vaiA(int pos) {//da l'indice del nodo numero pos
        int indicePos = inizio;
        for (int i = 0; i < pos; i++) {
            indicePos = matr[NEXT][indicePos];
        }
        return indicePos;
    }

    private void swap(int[] a, int x, int y) {
        int supporto = a[x];
        a[x] = a[y];
        a[y] = supporto;
    }

    private void maxHeapify(int[] a, int i,int size) {
        int l = 2*i+1;
        int r = 2*i+2;
        int largest = i;
        if (l < size && a[l] > a[largest]) {
            largest = l;
        }
        if (r < size && a[r] > a[largest]) {
            largest = r;
        }
        if (largest != i) {
            swap(a, largest, i);
            maxHeapify(a, largest,size);
        }
    }

    private void buildMaxHeap(int[] arr,int size) {
        for (int i = size / 2; i >= 0; i--) {
            maxHeapify(arr, i,size);
        }
    }

    private int[] heapMaxSort(int[] unsorted) {
        int size = unsorted.length;
        buildMaxHeap(unsorted,size);
        for (int i = size - 1; i >= 0; i--) {
            swap(unsorted, 0, i);
            size--;
            maxHeapify(unsorted, 0,size);
        }
        return unsorted;
    }
}
