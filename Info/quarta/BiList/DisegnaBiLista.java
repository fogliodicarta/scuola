package com.company;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class DisegnaBiLista extends Application {
    static int[] arr;
    static final double WIDTH = 1200;
    static final double HEIGHT = 600;
    static final String BACK = "#7FDBFF";
    static final String BORDO = "#001f3f";
    static final String FONT = "#001f3f";

    public static void main(String[] args, int[] array) {
        arr = array;
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("BiList");
        Group root = new Group();

        Scene scene = new Scene(root, WIDTH, HEIGHT, Color.web(BACK));//#12AC04 VERDE #F2B308 GIALLO
        dis(root);
        //primaryStage.setFullScreen(true);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private void freccia(Group root, double larg, double inizio, double fine, double y) {
        Color arrowColor = Color.web("#001f3f");//blu chiaro #0074D9 navy #001f3f
        Line corpo = new Line(inizio, y, fine, y);
        corpo.setFill(arrowColor);
        corpo.setStroke(arrowColor);
        corpo.setStrokeWidth(larg / 10);

        Line su = new Line(fine, y, fine - (fine - inizio) / 4, y - larg / 5);
        su.setFill(arrowColor);
        su.setStroke(arrowColor);
        su.setStrokeWidth(larg / 10);

        Line giu = new Line(fine, y, fine - (fine - inizio) / 4, y + larg / 5);
        giu.setFill(arrowColor);
        giu.setStroke(arrowColor);
        giu.setStrokeWidth(larg / 10);

        Line su1 = new Line(inizio, y, inizio + (fine - inizio) / 4, y - larg / 5);
        su1.setFill(arrowColor);
        su1.setStroke(arrowColor);
        su1.setStrokeWidth(larg / 10);

        Line giu1 = new Line(inizio, y, inizio + (fine - inizio) / 4, y + larg / 5);
        giu1.setFill(arrowColor);
        giu1.setStroke(arrowColor);
        giu1.setStrokeWidth(larg / 10);

        root.getChildren().add(corpo);
        root.getChildren().add(su);
        root.getChildren().add(giu);
        root.getChildren().add(su1);
        root.getChildren().add(giu1);
    }

    private void dis(Group root) {
        int nLinee = (int) Math.ceil(arr.length / 6.0);//6 è il numero di nodi da disporre per riga se i nodi sono fino a 18
        double largRett = WIDTH / 12;
        int nodiPerLinea = 6;
        int nCifre = 0;
        if (nLinee > 3) {
            largRett = largRett * 3 / nLinee;
            nodiPerLinea = nLinee * 2;
        }
        double largFont = largRett * 0.7;
        double y = (HEIGHT - (largRett * nLinee) - (largRett / 2 * (nLinee - 1))) / 2;
        int indice = 0;
        for (int i = 0; i < nLinee; i++) {
            double x = largRett / 2;

            for (int j = 0; indice < arr.length && j < nodiPerLinea; j++, indice++) {
                if(j==0 && i>0){
                    freccia(root,largRett,x-largRett,x,y+largRett/2);
                }
                Rectangle rett = new Rectangle(x, y, largRett, largRett);
                rett.setFill(Color.web(BACK));
                rett.setStroke(Color.web(BORDO));//#FB0202 ROSSO  #12AC04 VERDE
                rett.setStrokeWidth(largRett / 10);
                root.getChildren().add(rett);
                nCifre = String.valueOf(arr[indice]).length();
                Text testo = new Text(x + (largRett - largFont / 2.25) / (2 * nCifre), y + (largRett + largFont) / 2, "" + arr[indice]);
                testo.setFont(Font.font("Impact", largFont - (largRett / 14) * nCifre));
                testo.setFill(Color.web(FONT));
                root.getChildren().add(testo);
                freccia(root, largRett, x + largRett, x + largRett * 2, y + largRett / 2);
                x += largRett * 2;
            }
            y += largRett * 3 / 2;
        }
    }
}
