package com.company;


import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] max = new int[]{1,10,3,9,12,4,22,21};
        HeapTree albaro = new HeapTree(max);
        albaro.buildMaxHeap();
        albaro.printaHeap();
        System.out.println(Arrays.toString(albaro.heapMaxSort()));

        int[] min = new int[]{8,10,200,30,400,5,7,8,42,34};
        HeapTree albaro2 = new HeapTree(min);
        albaro2.buildMinHeap();

        System.out.println(albaro2);
        albaro2.insertMin(0);
        albaro2.deleteMin(1);
        albaro2.heapMaxSort();
        System.out.println(albaro2);
        albaro2.disegna(args);

    }
}
