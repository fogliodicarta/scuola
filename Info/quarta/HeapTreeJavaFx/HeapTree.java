package com.company;

import java.util.Arrays;

public class HeapTree {
    private int[] arr;
    private int size;

    HeapTree(int[] arr) {
        this.arr = arr;
        this.size = arr.length;
    }

    public int parent(int i) {
        return (i - 1) / 2;
    }

    public int left(int i) {
        return (2 * i) + 1;
    }

    public int right(int i) {
        return (2 * i) + 2;
    }

    public boolean isLeaf(int i) {
        return left(i) >= size;
    }

    public int elementAt(int i){
        return arr[i];
    }

    public void insertMax(int val) {
        int[] arr2 = new int[size + 1];
        for (int j = 0; j < size; j++) {
            arr2[j] = arr[j];
        }
        arr2[size++] = val;
        arr = arr2;
        buildMaxHeap();
    }

    public void insertMin(int val) {
        int[] arr2 = new int[size + 1];
        for (int j = 0; j < size; j++) {
            arr2[j] = arr[j];
        }
        arr2[size++] = val;
        arr = arr2;
        buildMinHeap();
    }

    public void deleteMax(int index){
        arr[index]=arr[--size];
        int[] arr2 = new int[size];
        for (int j = 0; j < size; j++) {
            arr2[j] = arr[j];
        }
        arr=arr2;
        buildMaxHeap();
    }

    public void deleteMin(int index){
        arr[index]=arr[--size];
        int[] arr2 = new int[size];
        for (int j = 0; j < size; j++) {
            arr2[j] = arr[j];
        }
        arr=arr2;
        buildMinHeap();
    }




    private void swap(int[] a, int x, int y) {
        int supporto = a[x];
        a[x] = a[y];
        a[y] = supporto;
    }

    private void maxHeapify(int[] a, int i) {
        int l = left(i);
        int r = right(i);
        int largest = i;
        if (l < size && a[l] > a[largest]) {
            largest = l;
        }
        if (r < size && a[r] > a[largest]) {
            largest = r;
        }
        if (largest != i) {
            swap(a, largest, i);
            maxHeapify(a, largest);
        }
    }

    private void minHeapify(int[] a, int i) {
        int l = left(i);
        int r = right(i);
        int smallest = i;
        if (l < size && a[l] < a[smallest]) {
            smallest = l;
        }
        if (r < size && a[r] < a[smallest]) {
            smallest = r;
        }
        if (smallest != i) {
            swap(a, smallest, i);
            minHeapify(a, smallest);
        }
    }

    public void buildMaxHeap() {
        for (int i = size / 2; i >= 0; i--) {
            maxHeapify(arr, i);
        }
    }

    public void buildMinHeap() {
        for (int i = size / 2; i >= 0; i--) {
            minHeapify(arr, i);
        }
    }

    public int[] heapMaxSort() {
        buildMaxHeap();
        for (int i = size - 1; i >= 0; i--) {
            swap(arr, 0, i);
            size--;
            maxHeapify(arr, 0);
        }
        return arr;
    }

    public int[] heapMinSort() {
        buildMinHeap();
        for (int i = size - 1; i >= 0; i--) {
            swap(arr, 0, i);
            size--;
            minHeapify(arr, 0);
        }
        return arr;
    }


    @Override
    public String toString() {
        return Arrays.toString(arr);
    }

    private void stampaBlank(int s) {
        for (int j = 0; j < s; j++) {
            System.out.print(' ');
        }
    }

    public void printaHeap() {
        int h = (int) Math.ceil(Math.log(size + 1) / Math.log(2) - 1);
        int stampate = 0;
        int nodo = 0;
        for (int i = 0; i <= h; i++) {
            stampaBlank((int) (Math.pow(2, h) - stampate) + h);
            for (int j = 0; nodo < size && j < Math.pow(2, i); j++, nodo++) {
                System.out.print(arr[nodo]);
                stampaBlank((int) (Math.pow(2, (h + 1) - i)) - j % 2);
            }
            System.out.println();
            stampate++;

            for (int linee = 0; i != h && linee < Math.pow(2, h - 1 - i); linee++) {
                stampaBlank((int) (Math.pow(2, h) - stampate) + h);
                for (int y = 0; y < Math.pow(2, i); y++) {
                    System.out.print('/');
                    stampaBlank(1 + 2 * linee);
                    System.out.print('\\');
                    stampaBlank((int) (Math.pow(2, (h + 1) - i) - linee * 2 - i * 2) + 2);

                }
                System.out.println();
                stampate++;
            }

        }

    }


    public void disegna(String args[]) {
        DisegnoHeap.main(args, arr);
    }

}
