package com.company;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class DisegnoHeap extends Application {
    private static int[] nodi;
    private final static double WIDTH = 1300;
    private final static double HEIGTH = 600;
    private final static double RADIUS = 30;
    private final static double MIN_ANGOLO = 15;//IN GRADI
    private final static double MAX_ANGOLO = 170;//IN GRADI
    private final static double MIN_LINEA = 50;
    private static int hAlbero;
    private static double widthTesto = 20;

    public static void main(String[] args, int[] myArr) {
        nodi = myArr;
        hAlbero = (int) Math.ceil(Math.log(nodi.length + 1) / Math.log(2) - 1);
        launch(args);
    }

    private static void nodeToRoot(Group root, double x, double y, int index) {

        int livello = (int) Math.floor((Math.log(index + 1) / Math.log(2)) - 1);
        double angolo = livello == -1 ? MAX_ANGOLO / 2 : (MIN_ANGOLO + MIN_ANGOLO * (hAlbero - livello)) / 2;
        double l = MIN_LINEA + MIN_LINEA * (hAlbero - livello);
        if (2 * index + 1 < nodi.length) {//LEFT
            double endX = x - l * Math.sin(angolo * Math.PI / 180);
            double endY = y + l * Math.cos(angolo * Math.PI / 180);
            root.getChildren().add(new Line(x, y, endX, endY));
            nodeToRoot(root, endX, endY, 2 * index + 1);
        }
        if (2 * index + 2 < nodi.length) {//RIGHT
            double endX = x + l * Math.sin(angolo * Math.PI / 180);
            double endY = y + l * Math.cos(angolo * Math.PI / 180);
            root.getChildren().add(new Line(x, y, endX, endY));
            nodeToRoot(root, endX, endY, 2 * index + 2);
        }
        Text num = new Text(x - widthTesto / 3 * String.valueOf(nodi[index]).length(), y + widthTesto / 3, "" + nodi[index]);
        num.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, widthTesto));//scrivi nella documentazione
        num.setFill(Color.ANTIQUEWHITE);
        Circle cerchio = new Circle(x, y, RADIUS, Color.web("0700B3"));
        cerchio.setStroke(Color.web("03DAC5").darker());
        cerchio.setStrokeWidth(5);
        root.getChildren().add(cerchio);
        root.getChildren().add(num);

    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Heap Tree");
        Group root = new Group();

        Scene scene = new Scene(root, WIDTH, HEIGTH, Color.web("6200EE"));

        double xInit = WIDTH / 2;
        double yInit = RADIUS * 1.5;

        nodeToRoot(root, xInit, yInit, 0);

        primaryStage.setFullScreen(true);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
