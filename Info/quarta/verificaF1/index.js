$(document).ready(() => {//onload
	$(".nascosto").hide();//nasconde le tabelle che verrano mostrate dopo la richiesta ajax
	$("#test").click(() => {//mostra il messaggio di benvenuto
		richiesta("", (risposta) => {
			$("#1 span.risultati").text(risposta);
		});
	});

	$("#elenco").click(() => {//mostra il raw data dell'elenco di costruttori
		richiesta("costruttori", (risposta) => {
			$("#2 span.risultati").text(JSON.stringify(risposta));
		});
	});

	$("#lista").click(() => {//mostra una lista non ordinata dei costruttori
		richiesta("costruttori", (risposta) => {
			$("#3 ul").text("");//resetto la ul in modo che i nuovi dati non vengano aggiunti a quelli precedenti
			for (var i = 0; i < risposta.length; i++) {//creo la lista con i costruttori
				$("#3 ul").append("<li> " + risposta[i].nomeCostruttore + " (" + risposta[i].nazioneCostruttore + ") punti:" + risposta[i].puntiCostruttore + "</li>");
			}
			;
		});
	});

	$("#tabella").click(() => {//mostra una lista di tutti i piloti e dei loro dati
		richiesta("piloti", (risposta) => {
			//resetto la tabella così non continuo ad aggiungere i piloti alla tabella esistente
			$("#4 table").html("<table class='nascosto'><tr><th>Nome</th><th>Cognome</th><th>Nazionalità</th><th>Data di nascita</th>	</tr></table>");
			$("#4 table").show();//mostro la tabella
			for (var i = 0; i < risposta.length; i++) {//aggiungo i dati dei piloti nella tabella
				let nome = risposta[i].nomePilota;
				let cognome = risposta[i].cognomePilota;
				let nazionalita = risposta[i].nazionalita;
				let data = risposta[i].dataNascita;
				$("#4 table").append("<tr><td>" + nome + "</td>" + "<td>" + cognome + "</td>" + "<td>" + nazionalita + "</td>" + "<td>" + data + "</td></tr>");
			}
		});
	});

	$("#pilotitext").click(() => {//mostra i piloti di un team specificato nel form
		var idcostr = $("#formteam").val();//prendo il valore dal form
		if (parseInt(idcostr) > 0 && parseInt(idcostr) < 6) {//controllo che il valore preso dal form sia un idCostruttore valido
			richiesta("/campionati/2018/pilotiDeiTeams/" + idcostr, (risposta) => {
				//resetto la tabella così non continuo ad aggiungere i piloti alla tabella esistente
				$("#5 table").html("<table class='nascosto'><tr><th>Nome</th><th>Cognome</th><th>Nazionalità</th><th>Data di nascita</th>	</tr></table>");
				$("#5 table").show();//mostro la tabella 
				for (var i = 0; i < risposta.length; i++) {//aggiungo i dati dei piloti nella tabella
					let nome = risposta[i].nomePilota;
					let cognome = risposta[i].cognomePilota;
					let nazionalita = risposta[i].nazionalita;
					let data = risposta[i].dataNascita;
					$("#5 table").append("<tr><td>" + nome + "</td>" + "<td>" + cognome + "</td>" + "<td>" + nazionalita + "</td>" + "<td>" + data + "</td></tr>");
				}
			});
		}
	});

	$("#selettore").change(() => {//mostra i piloti di un team specificato nel selector
		var idcostr = $("#selettore").val();
		richiesta("/campionati/2018/pilotiDeiTeams/" + idcostr, (risposta) => {
			//resetto la tabella così non continuo ad aggiungere i piloti alla tabella esistente
			$("#6 table").html("<table class='nascosto'><tr><th>Nome</th><th>Cognome</th><th>Nazionalità</th><th>Data di nascita</th>	</tr></table>");
			$("#6 table").show();//mostro la tabella
			for (var i = 0; i < risposta.length; i++) {//aggiungo i dati dei piloti nella tabella
				let nome = risposta[i].nomePilota;
				let cognome = risposta[i].cognomePilota;
				let nazionalita = risposta[i].nazionalita;
				let data = risposta[i].dataNascita;
				$("#6 table").append("<tr><td>" + nome + "</td>" + "<td>" + cognome + "</td>" + "<td>" + nazionalita + "</td>" + "<td>" + data + "</td></tr>");
			}
		});
	});
});

function richiesta(ric, fine) {//funzione chiamata per tutte le richieste ajax visto che sono tutte richieste get allo stesso server
	//ric è la richiesta da aggiungere all'url base del server,fine è la funzione da eseguire una volta arrivati i dati
	$.ajax({
		url: "http://192.168.4.1:8088/" + ric,
		type: "GET",
		success: fine,
		error: () => {
			console.log("errore");
		}
	});
}