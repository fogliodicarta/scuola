
public class Main {
    public static void aspetta(){ //aspetta 1 secondo, creata per poter verificare il funzionamento della data/ora
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {


        //creo un conto bancario ed eseguo dei depositi e prelievi
        BankAccount conto3 = new BankAccount("Luigi Verdi");
        conto3.accredito(1200);
        aspetta();
        conto3.prelievo(55);
        aspetta();
        aspetta();
        conto3.accredito(21);
        aspetta();
        conto3.prelievo(1111);

        conto3.getEstrattoConto(); //visualizzo l'estratto conto

        System.out.println(conto3.getSaldo()); //visualizzo il saldo del conto bancario
    }
}
