﻿import java.util.Iterator;
import java.util.Stack;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.Scanner;

public class BankAccount {
    Scanner tastiera = new Scanner(System.in);
    final static long saldoMinimo = -500;

    private String intestatario; //nome e cognome del proprietario
    private long number;    // numero del conto
    private long saldo;   //saldo
    private Stack<Operation> ops; //lista di tutte le operazioni eseguite sul conto

    BankAccount(String intestatario) {
        this.intestatario = intestatario;
        number = CodeGenerator.newCode();
        saldo = 0;
        ops = new Stack<Operation>();
    }

    public class Operation {
        private String op; //tipo di operazione
        private long amount; //quantità di soldi depositati o prelevati
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy 'alle' HH:mm:ss");
        LocalDateTime now;

        Operation(String op, long amount) {
            this.op = op;
            this.amount = amount;
            now = LocalDateTime.now();
        }

        @Override
        public String toString() {
            return "Account id:" + number + ", " + intestatario + ", " + op + " di " + amount + "€ il " + now.format(dtf);
        }

    }

    private static class CodeGenerator {
        static long currentCode = 0;

        private static long newCode() {
            return currentCode++;
        }
    }//genera un codice del conto bancario diverso per ogni utente


    public Iterator iterator() { //iteratore della lista di azioni
        return ops.iterator();
    }

    public long getSaldo() { //restituisce il saldo
        return saldo;
    }

    public Operation getLastOp() { //restituisce l'ultima operazione effettuata sul conto
        return ops.lastElement();
    }

    public String getLastOpInfo() { //restituisce una stringa contenente informazioni sull'ultima azione e sul saldo
        return this.getLastOp().toString() + " | Estratto conto:" + this.getSaldo() + "€ |";
    }
    
    public void getEstrattoConto(){ //stampa a video l'estrattoconto
        for (Iterator<Operation> iter = iterator(); iter.hasNext(); ) {
            Operation a =  iter.next();
            System.out.println(a);
        }
    }

    public void accredito(long amount) { //deposita nel conto bancario
        System.out.println(String.format("%s %d € SI/NO", "Sei sicuro di voler effettuare un accredito di", amount));
        String conferma = tastiera.next();
        if (conferma.equals("SI")) {
            saldo += amount;
            ops.addElement(new Operation("accredito", amount));
        } else {
            System.out.println("Operazione annullata");
        }
    }

    public void prelievo(long amount) { //preleva dal conto bancario se il conto va in rosso di al masimo 500 euro
        System.out.println(String.format("%s %d € SI/NO", "Sei sicuro di voler effettuare un prelievo di", amount));
        String conferma = tastiera.next();
        if (conferma.equals("SI") && saldo-amount >= saldoMinimo) {
            saldo -= amount;
            ops.addElement(new Operation("prelievo", amount));
        } else {
            System.out.println("Operazione annullata");
        }
    }
}

