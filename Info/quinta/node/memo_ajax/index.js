let express = require('express');
const fs = require('fs');

let app = express();
let list = [];
let objetto;

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));

app.get('/', function (req, res) {
  fs.exists('listaMemo.json', function(exists) {
    if (exists) {
        console.log("yes file exists");
        fs.readFile('listaMemo.json', function readFileCallback(err, data) {
            if (err) {
                console.log(err);
            } else {
               objetto = JSON.parse(data);
               list=objetto;
               res.render('index',
               { lista: list }
             );
            }
        });
    } else {
        console.log("file not exists");
        fs.writeFile('listaMemo.json', JSON.stringify({}), 'utf8', ()=>{"file creato get"});
        res.render('index',
        { lista: list }
      );
    }
});
});
app.post('/creamemo', (req, res) => {
  if (req.body.nome == "") {
    req.body.nome = "Memo"
  }
  list.push({ titolo: req.body.nome, testo: null });
  fs.writeFile('listaMemo.json', JSON.stringify(list), 'utf8', ()=>{"file salvato post"});
  //console.log('creato',list.length-1,list);
  console.log(list)
  console.log(`
  `)
  res.json({
    index:list.length-1,
    title:req.body.nome
  });
  
});
app.put('/aggiornamemo', (req, res) => {
  let indice = parseInt(req.body.indicememo);
  list[indice].testo = req.body.testomemo;
  console.log("aggiornato",indice,list);
  fs.writeFile('listaMemo.json', JSON.stringify(list), 'utf8', ()=>{"file salvato put"});
  res.json({
    index:req.body.indicememo,
    title:list[indice].titolo
  });
});

app.delete('/deletememo', (req, res) => {
  titolorimosso=list[parseInt(req.query.id)].titolo;
  console.log("prerimozione",list)
  list.splice(parseInt(req.query.id), 1);
  console.log("doporimozione",list)
  //console.log("rimosso", req.query.id,list);
  fs.writeFile('listaMemo.json', JSON.stringify(list), 'utf8', ()=>{"file salvato delete"});
  res.json({
    index:parseInt(req.query.id),
    title:titolorimosso
  });
});




app.listen(3000, () => console.log('Server ready'));