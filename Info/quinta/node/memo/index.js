let express = require('express');

let app = express();
let list=[];
app.use(express.static(__dirname  + '/public'));
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));

app.get('/', function(req, res){
  res.render('index',
    {lista:list}
  );
});
app.get('/deletememo',(req,res)=>{
  list.splice(parseInt(req.query.id),1);
  console.log("rimosso",req.query.id);
  res.render('index',
  {lista:list}
  );
});

app.post('/creamemo',(req,res)=>{
  console.log('creamemo:', req.body.nome);
  if(req.body.nome==""){
    req.body.nome="Memo"
  }
  list.push({titolo:req.body.nome,testo:null});
  res.render('index',
    {lista:list}
  );
});

app.post('/aggiornamemo',(req,res)=>{
  console.log('aggiornamemo:',req.body);
  let indice = parseInt(req.body.indicememo);
  list[indice].testo=req.body.testomemo;
  console.log(list);
  res.render('index',
    {lista:list}
  );
});




app.listen(3000, () => console.log('Server ready'));