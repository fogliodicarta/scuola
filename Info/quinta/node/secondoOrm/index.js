const Sequelize = require('sequelize');

var sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5, // Maximum number of connection in pool
      min: 0, // min ...
      idle: 10000 // The maximum time, in milliseconds, that a connection can be idle before being released
    },
});

var User = sequelize.define('user', {
    firstName: {
      type: Sequelize.STRING,
      field: 'first_name' // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    lastName: {
      type: Sequelize.STRING,
      field: 'last_name'
    }
  }, {
    freezeTableName: true // Model tableName will be the same as the model name, not plural
  });

User.sync({force: true}).then(function () {
  // Table created
  return User.create({
    firstName: 'Steve3',
    lastName: 'Steve3'
  });
}).then(()=>{
  return User.create({
    firstName: 'Steve4',
    lastName: 'Steve4'
  });
}).then(()=>{
  return User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
  });
});



