let selettore,data,sino,prompt,bottone;
let current_option;
let richiesta1;
let richiesta2;
let risposta;
window.onload=()=>{
    
    console.log('onload iniziato');
    
    selettore=$('#selettore');
    data=$('#data');
    sino=$('#sino');
    prompt=$('#prompt');
    bottone=$('#bottone');

    richiesta1=$('#richiesta1');
    richiesta2=$('#richiesta2');
    risposta=$('#risposta');

    selettore.val('0');
    data.change(handleData);

    selettore.change(()=>{
        current_option=selettore.children("option:selected").val();
        data.fadeOut(300);
        sino.fadeOut(300);
        prompt.fadeOut(300);
        bottone.fadeOut(300);
        richiesta2.fadeOut(300);
        risposta.fadeOut(300);

        switch(parseInt(current_option)){
            case 0://smarrimento
                data.animate({height:'show',width:'show'});
                richiesta2.text('Inserisci la data di ordine del prodotto');
                richiesta2.animate({height:'show',width:'show'});
                data.change(handleData);
                break;
            case 1://danneggiamento
                $("#si").prop("checked", false);
                $("#no").prop("checked", false);
                sino.animate({height:'show',width:'show'});
                richiesta2.text('Il prodotto è danneggiato?');
                richiesta2.animate({height:'show',width:'show'});
                sino.change(handleSino);
                break;
            case 2://conformita
                risposta.text('Invieremo il corriere a prelevare il prodotto e spediremo un nuovo articolo');
                risposta.animate({height:'show',width:'show'});
                break;
            case 3://altro
                prompt.animate({height:'show',width:'show'});
                bottone.animate({height:'show',width:'show'});
                break;
        }
    });
}

function handleData(){
    console.log(data[0].value);
    let dif = date_diff(data[0].value);
    console.log(dif);
    if (dif>7) {
        risposta.text('Provvederemo a contattare il corriere; riceverai una notifica entro 24 ore');
    }else{
        risposta.text('Attendi fino allo scadere del 7° giorno e nel caso ricontattaci');
    }
    risposta.animate({height:'show',width:'show'});
}
function handleSino(){
    console.log(sino);
    let v=$("#ilform input[type='radio']:checked").val();
    console.log(v);
    if (v=='si') {
        risposta.text('Provvederemo a rispedire il prodotto, puoi gettare quello in tuo possesso');
    }else{
        risposta.text('Invieremo il corriere a prelevare il prodotto danneggiato e spediremo un nuovo articolo');
    }
    risposta.animate({height:'show',width:'show'});
}
function date_diff(date1) {
    dt1 = new Date(date1);
    dt2 = new Date();
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
