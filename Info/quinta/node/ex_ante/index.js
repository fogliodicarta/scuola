let express = require('express');
let app = express();

app.set('view engine','ejs');
app.use(express.static(__dirname  + '/public'));
app.use(express.urlencoded({extended:true}));

app.get('/',(req,res)=>{
    res.render('index');
});

app.post('/altro',(req,res)=>{
    console.log(req.body.prompt);
    res.send('Grazie provvederemo a esaminare il tuo problema al più presto');
});

app.listen(3000,()=>{
    console.log('server avviato su localhost:3000');
});