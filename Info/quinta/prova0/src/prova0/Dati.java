/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prova0;

/**
 *
 * @author degobbi1
 */
public class Dati {
   private static String user; 
   private static String password;
   private static String host;
   private static String port;
   private static String action;
   
   public static void init(String user, String password, String host,String port,String action){
       Dati.user=user;
       Dati.password=password;
       Dati.host=host;
       Dati.port=port;
       Dati.action=action;
   }
   public static String getuser(){
       return user;
   }
   public static String getpassword(){
       return password;
   }
   public static String gethost(){
       return host;
   }
   public static String getport(){
       return port;
   }
   public static String gettutto(){
       return String.format("%s,%s,%s,%s",user,password,host,port);
   }
   public static String getaction(){
      return action;
   }
}
