/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prova0;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author degobbi1
 */
public class Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    public String path;
    public FileChooser fileChooser = new FileChooser();

    @FXML
    public Text chosen;

    @FXML
    public ToggleGroup action;
    @FXML
    public TextField user;
    @FXML
    public TextField password;
    @FXML
    public TextField host;
    @FXML
    public TextField port;
    @FXML
    public TextField schermo;

    public String lastMex;

    @FXML
    public ListView lista;

    public void scegliFile() {
        try {
            fileChooser.setTitle("Open Resource File");
            File scelto = fileChooser.showOpenDialog(Prova0.stage);
            chosen.setText(scelto.toString());
            path = scelto.toString();
        } catch (Exception e) {
        }
    }

    public void onPressInvio() throws ProtocolException, IOException, Exception {
        String[] temp = null;
        RadioButton selectedRadioButton = (RadioButton) action.getSelectedToggle();
        String toogleGroupValue = selectedRadioButton.getText();
        Dati.init(user.getText(), password.getText(), host.getText(), port.getText(), toogleGroupValue);
        ConnectionHandler ch = new ConnectionHandler();
        BufferedReader reader;
        int httpCode = 0;
        try {
            httpCode = ConnectionHandler.ping();
            System.out.println(httpCode);
        } catch (Exception e) {
            System.out.println("Connessione fallita, controlla username e password");
            launchErrore("Connessione fallita", "La connessione è fallita, controlla username, password e le informazioni del server");
        }
        if (httpCode == 200) {
            try {
                reader = new BufferedReader(new FileReader(path));
                String line = reader.readLine();
                temp = ch.invia(line);
                lista.getItems().add(String.format("Nome:%s\tActivation State:%s\tChannel State:%s", temp[0], temp[1], temp[2]));

                while (line != null) {
                    // read next line
                    line = reader.readLine();
                    try {
                        if (line != null) {
                            temp = ch.invia(line);
                            lista.getItems().add(String.format("Nome:%s\tActivation State:%s\tChannel State:%s", temp[0], temp[1], temp[2]));
                        }
                    } catch (Exception e) {
                        System.out.println("link invalido");
                        //launchErrore("Link invalido", "Il link del server è errato o il server non è disponibile");
                    }
                }
                reader.close();
            } catch (Exception e) {
                System.out.println("errore nella lettura");
                launchErrore("errore nella lettura", "C'è stato un errore durante la lettura del file scegli un altro file e riprova");
            }
        } else {
            System.out.println("Errore,controlla username e password");
        }

    }

    public void launchErrore(String mex, String mex2) throws IOException {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Errore!");
        alert.setHeaderText(mex);
        alert.setContentText(mex2);
        alert.showAndWait();
    }

}
