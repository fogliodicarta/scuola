package prova0;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Base64;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.xml.sax.InputSource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author degobbi1
 */
public class ConnectionHandler {

    public org.w3c.dom.Document loadXMLFromString(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }
    private URL url;
    private String fmat;

    public ConnectionHandler() {
        fmat = "http://%s:%s/AdapterFramework/ChannelAdminServlet?&party=*&service=*&channel=%s&action=%s";
    }

    public String[] invia(String row) throws MalformedURLException, ProtocolException, IOException, Exception {
        String temp;
        temp = String.format(fmat, Dati.gethost(), Dati.getport(), row, Dati.getaction());
        url = new URL(temp);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("x-auth-token", "kfksj48sdfj4jd9d");
        String userpass = Dati.getuser() + ":" + Dati.getpassword();
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
        con.setRequestProperty("Authorization", basicAuth);
        con.setConnectTimeout(2000);
        con.setReadTimeout(2000);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        String xmls = content.toString();
        int channelIndex=xmls.indexOf(">",xmls.indexOf("<ChannelName>"))+1;
        String channelName = xmls.substring(channelIndex, xmls.indexOf("<",channelIndex));
        //System.out.println(channelName);
        int asIndex=xmls.indexOf(">",xmls.indexOf("<ActivationState>"))+1;
        String as = xmls.substring(asIndex, xmls.indexOf("<",asIndex));
        //System.out.println(as);
        int csIndex=xmls.indexOf(">",xmls.indexOf("<ChannelState>"))+1;
        String cs = xmls.substring(csIndex, xmls.indexOf("<",csIndex));
        //System.out.println(cs);
        return new String[]{channelName,as,cs};
    }
    public static int ping() throws IOException{
        String temp2;
        temp2= String.format("http://%s:%s/sap/bc/ping",Dati.gethost(),Dati.getport());
        URL urlping= new URL(temp2);
        HttpURLConnection con = (HttpURLConnection) urlping.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("x-auth-token", "kfksj48sdfj4jd9d");
        String userpass = Dati.getuser() + ":" + Dati.getpassword();
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
        con.setRequestProperty("Authorization", basicAuth);
        con.setConnectTimeout(2000);
        con.setReadTimeout(2000);
        return con.getResponseCode();
    }
}