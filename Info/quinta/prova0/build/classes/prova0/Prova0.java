/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prova0;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author degobbi1
 */
public class Prova0 extends Application {
    public static Stage stage;
    @Override
    public void start(Stage primaryStage) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("start.fxml"));
        
        stage=primaryStage;
        primaryStage.setTitle("SAP gestore canali");
        primaryStage.setScene(new Scene(root, 750, 650));
        primaryStage.show();
        
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
