# School

Le tabelle del DB sono, omonime ai fial odt, decrivono il DB.

- Usare i file odt per creare in modo opportuno i file csv
- Creare il database in MariaDB rispetttando i nomi dei file ed i nomi delle tabelle (nomi dei file odt), usare opportuni tipi di dati per i campi.
- dare i permessi `ALL` sul database all'utente `school` con  password `school@2020`
- importare le tabelle dai file csv
- aggiungere i **vincoli**: chiavi primarie, chiavi esterne, `NOT NULL` ed integrità referenziale con clausole (`ON UPDATE` e `ON DELETE`)
- aggiungere due valutazioni a studenti esistenti

## query 

1. Si trovi la media dei voti per materia su due colonne: `average` e `sobject`.
1. Si trovino gli studenti che in Matematica hanno almeno un voto superiore alla media dei voti in Matematica di tutti gli studenti.
3. Si trovino gli studenti nati nel `2001` che hanno ricevuto almeno una valutazione insufficiente in Informatica.
4. Quali sono gli studenti di sesso femminile ad avere la media migliore (si tenga comto del fatto che il voto per materia è dato dalla media dei voti).