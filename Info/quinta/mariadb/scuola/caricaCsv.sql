LOAD DATA LOCAL INFILE '/home/fogliodicarta/Scrivania/scuola/Info/quinta/mariadb/scuola/csv/studenti.csv'
INTO TABLE Students
 FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA LOCAL INFILE '/home/fogliodicarta/Scrivania/scuola/Info/quinta/mariadb/scuola/csv/materie.csv'
INTO TABLE Subjects
 FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA LOCAL INFILE '/home/fogliodicarta/Scrivania/scuola/Info/quinta/mariadb/scuola/csv/voti.csv'
INTO TABLE Grades
 FIELDS TERMINATED BY ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;
