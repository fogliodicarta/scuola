-- MySQL dump 10.16  Distrib 10.1.43-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: School
-- ------------------------------------------------------
-- Server version	10.1.43-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Grades`
--

DROP TABLE IF EXISTS `Grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Grades` (
  `idStudent` int(11) NOT NULL,
  `grade` int(11) NOT NULL,
  `idSubject` int(11) NOT NULL,
  `idGrade` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idGrade`),
  KEY `idStudent` (`idStudent`),
  KEY `idSubject` (`idSubject`),
  CONSTRAINT `Grades` FOREIGN KEY (`idStudent`) REFERENCES `Students` (`idStudent`),
  CONSTRAINT `Grades_ibfk_1` FOREIGN KEY (`idStudent`) REFERENCES `Students` (`idStudent`),
  CONSTRAINT `Grades_ibfk_2` FOREIGN KEY (`idSubject`) REFERENCES `Subjects` (`idSubject`),
  CONSTRAINT `Grades_ibfk_3` FOREIGN KEY (`idStudent`) REFERENCES `Students` (`idStudent`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Grades_ibfk_4` FOREIGN KEY (`idSubject`) REFERENCES `Subjects` (`idSubject`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Grades`
--

LOCK TABLES `Grades` WRITE;
/*!40000 ALTER TABLE `Grades` DISABLE KEYS */;
INSERT INTO `Grades` VALUES (1,6,1,1),(2,4,1,2),(3,7,1,3),(4,8,1,4),(5,10,1,5),(6,4,1,6),(7,6,1,7),(8,5,1,8),(9,8,1,9),(10,7,1,10),(11,6,1,11),(12,6,1,12),(13,5,1,13),(14,6,1,14),(15,6,1,15),(16,7,1,16),(17,5,1,17),(18,6,1,18),(19,4,1,19),(20,6,1,20),(21,6,1,21),(22,7,1,22),(23,6,1,23),(1,6,2,24),(2,6,2,25),(3,6,2,26),(4,6,2,27),(5,4,2,28),(6,6,2,29),(7,6,2,30),(8,8,2,31),(9,8,2,32),(10,8,2,33),(11,6,2,34),(12,4,2,35),(13,6,2,36),(14,6,2,37),(15,6,2,38),(16,5,2,39),(17,6,2,40),(18,4,2,41),(19,6,2,42),(20,6,2,43),(21,4,2,44),(22,3,2,45),(23,2,2,46),(1,6,3,47),(2,6,3,48),(3,7,3,49),(4,5,3,50),(5,6,3,51),(6,4,3,52),(7,6,3,53),(8,6,3,54),(9,7,3,55),(10,6,3,56),(11,6,3,57),(12,6,3,58),(13,6,3,59),(14,6,3,60),(15,4,3,61),(16,4,3,62),(17,4,3,63),(18,5,3,64),(19,7,3,65),(20,7,3,66),(21,7,3,67),(22,8,3,68),(23,6,3,69),(1,6,4,70),(2,6,4,71),(3,4,4,72),(4,4,4,73),(5,4,4,74),(6,5,4,75),(7,10,4,76),(8,10,4,77),(9,9,4,78),(10,8,4,79),(11,8,4,80),(12,8,4,81),(13,5,4,82),(14,6,4,83),(15,6,4,84),(16,4,4,85),(17,4,4,86),(18,4,4,87),(19,5,4,88),(20,7,4,89),(21,6,4,90),(22,6,4,91),(23,6,4,92),(1,4,5,93),(2,5,5,94),(3,7,5,95),(4,7,5,96),(5,7,5,97),(6,8,5,98),(7,6,5,99),(8,6,5,100),(9,6,5,101),(10,4,5,102),(11,4,5,103),(12,4,5,104),(13,5,5,105),(14,10,5,106),(15,10,5,107),(16,9,5,108),(17,8,5,109),(18,8,5,110),(19,3,5,111),(20,4,5,112),(21,5,5,113),(22,6,5,114),(23,7,5,115),(1,6,1,116),(2,7,1,117),(3,8,1,118),(4,5,1,119),(5,10,1,120),(6,10,1,121),(7,9,1,122),(8,8,1,123),(9,8,1,124),(10,8,1,125),(11,5,1,126),(12,6,1,127),(13,6,1,128),(14,4,1,129),(15,4,1,130),(16,4,1,131),(17,5,1,132),(18,7,1,133),(19,6,1,134),(20,6,1,135),(21,6,1,136),(22,4,1,137),(23,6,1,138),(1,6,2,139),(2,6,2,140),(3,5,2,141),(4,6,2,142),(5,5,2,143),(6,6,2,144),(7,6,2,145),(8,4,2,146),(9,4,2,147),(10,4,2,148),(11,5,2,149),(12,7,2,150),(13,6,2,151),(14,6,2,152),(15,6,2,153),(16,4,2,154),(17,6,2,155),(18,6,2,156),(19,6,2,157),(20,6,2,158),(21,6,2,159),(22,6,2,160),(23,6,2,161),(1,5,3,162),(2,5,3,163),(3,6,3,164),(4,6,3,165),(5,4,3,166),(6,4,3,167),(7,4,3,168),(8,5,3,169),(9,7,3,170),(10,6,3,171),(11,6,3,172),(12,6,3,173),(13,4,3,174),(14,6,3,175),(15,6,3,176),(16,6,3,177),(17,6,3,178),(18,6,3,179),(19,6,3,180),(20,7,3,181),(21,6,3,182),(22,5,3,183),(23,6,3,184),(1,6,4,185),(2,4,4,186),(3,4,4,187),(4,4,4,188),(5,5,4,189),(6,7,4,190),(7,6,4,191),(8,6,4,192),(9,6,4,193),(10,4,4,194),(11,5,4,195),(12,5,4,196),(13,6,4,197),(14,6,4,198),(15,4,4,199),(16,4,4,200),(17,4,4,201),(18,5,4,202),(19,7,4,203),(20,6,4,204),(21,6,4,205),(22,6,4,206),(23,4,4,207),(1,6,5,208),(2,6,5,209),(3,6,5,210),(4,6,5,211),(5,6,5,212),(6,10,5,213),(7,7,5,214),(8,5,5,215),(9,6,5,216),(10,6,5,217),(11,4,5,218),(12,4,5,219),(13,4,5,220),(14,5,5,221),(15,7,5,222),(16,6,5,223),(17,6,5,224),(18,6,5,225),(19,4,5,226),(20,6,5,227),(21,6,5,228),(22,5,5,229),(23,6,5,230),(1,6,1,231),(2,6,1,232),(3,10,1,233),(4,7,1,234),(5,5,1,235),(6,6,1,236),(7,6,1,237),(8,4,1,238),(9,8,1,239),(10,8,1,240),(11,6,1,241),(12,6,1,242),(13,6,1,243),(14,4,1,244),(15,4,1,245),(16,4,1,246),(17,7,1,247),(18,7,1,248),(19,7,1,249),(20,7,1,250),(21,6,1,251),(22,6,1,252),(23,10,1,253),(1,7,2,254),(2,5,2,255),(3,6,2,256),(4,6,2,257),(5,4,2,258),(6,6,2,259),(7,6,2,260),(8,4,2,261),(9,4,2,262),(10,4,2,263),(11,5,2,264),(12,5,2,265),(13,6,2,266),(14,7,2,267),(15,7,2,268),(16,9,2,269),(17,10,2,270),(18,6,2,271),(19,6,2,272),(20,6,2,273),(21,4,2,274),(22,6,2,275),(23,7,2,276),(1,6,3,277),(2,6,3,278),(3,6,3,279),(4,6,3,280),(5,6,3,281),(6,4,3,282),(7,8,3,283),(8,5,3,284),(9,7,3,285),(10,6,3,286),(11,5,3,287),(12,6,3,288),(13,4,3,289),(14,6,3,290),(15,6,3,291),(16,6,3,292),(17,6,3,293),(18,6,3,294),(19,6,3,295),(20,7,3,296),(21,6,3,297),(22,5,3,298),(23,6,3,299),(1,6,4,300),(2,6,4,301),(3,6,4,302),(4,6,4,303),(5,5,4,304),(6,7,4,305),(7,6,4,306),(8,6,4,307),(9,6,4,308),(10,4,4,309),(11,5,4,310),(12,5,4,311),(13,6,4,312),(14,6,4,313),(15,4,4,314),(16,4,4,315),(17,4,4,316),(18,5,4,317),(19,7,4,318),(20,6,4,319),(21,3,4,320),(22,3,4,321),(23,4,4,322),(1,6,5,323),(2,6,5,324),(3,6,5,325),(4,3,5,326),(5,3,5,327),(6,10,5,328),(7,7,5,329),(8,5,5,330),(9,6,5,331),(10,6,5,332),(11,4,5,333),(12,4,5,334),(13,4,5,335),(14,5,5,336),(15,7,5,337),(16,6,5,338),(17,3,5,339),(18,3,5,340),(19,4,5,341),(20,6,5,342),(21,6,5,343),(22,5,5,344),(23,6,5,345);
/*!40000 ALTER TABLE `Grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Students`
--

DROP TABLE IF EXISTS `Students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Students` (
  `idStudent` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `surname` varchar(64) NOT NULL,
  `sex` enum('M','F') NOT NULL,
  `birth_year` year(4) NOT NULL,
  PRIMARY KEY (`idStudent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Students`
--

LOCK TABLES `Students` WRITE;
/*!40000 ALTER TABLE `Students` DISABLE KEYS */;
INSERT INTO `Students` VALUES (1,'Piero','Marin','M',2001),(2,'Andrea','Susa','M',2001),(3,'Giovanni','Amodeo','M',2001),(4,'Rossella','Rossi','F',1998),(5,'Paolo','Rossi','M',2002),(6,'Maria','Viva','F',2000),(7,'Marco','Pilastro','M',2001),(8,'Anna','Tavola','F',2001),(9,'Marco','Firma','M',2001),(10,'Angela','Roma','F',2001),(11,'Alessio','Furbo','M',2001),(12,'Pino','Tagliabirra','M',2001),(13,'Vanni','Nono','M',1999),(14,'Angelo','Careano','M',2001),(15,'Thomas','Guji','M',2001),(16,'Rino','Mattei','M',2001),(17,'Luca','Gava','M',2001),(18,'Josha','Tree','M',2001),(19,'Yin','Fin','F',2002),(20,'Guglielmo','Tello','M',2001),(21,'Anna','Testi','F',2001),(22,'Wilma','Winger','F',2001),(23,'Maria','Radulescu','F',2000);
/*!40000 ALTER TABLE `Students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subjects`
--

DROP TABLE IF EXISTS `Subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subjects` (
  `idSubject` int(11) NOT NULL,
  `name` varchar(16) NOT NULL,
  PRIMARY KEY (`idSubject`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subjects`
--

LOCK TABLES `Subjects` WRITE;
/*!40000 ALTER TABLE `Subjects` DISABLE KEYS */;
INSERT INTO `Subjects` VALUES (1,'Informatica'),(2,'Italiano'),(3,'Matematica'),(4,'Sistemi'),(5,'Storia');
/*!40000 ALTER TABLE `Subjects` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-17 16:06:33
