SELECT Students.name,surname,MAX(Grades.grade) FROM Students JOIN(Grades,Subjects) ON(Students.idStudent=Grades.idStudent AND Grades.idSubject=Subjects.idSubject) 
WHERE Subjects.name="Matematica"
GROUP BY Students.name,Students.surname
HAVING MAX(Grades.grade) > (SELECT AVG(Grades.grade) FROM Grades JOIN (Subjects) ON (Grades.idSubject=Subjects.idSubject) WHERE Subjects.name="Matematica" );
