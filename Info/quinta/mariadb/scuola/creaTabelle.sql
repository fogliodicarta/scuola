CREATE OR REPLACE TABLE Students(
  idStudent int NOT NULL,
  name varchar(64) NOT NULL,
  surname varchar(64) NOT NULL,
  sex ENUM('M','F') NOT NULL,
  birth_year YEAR NOT NULL,
  
  PRIMARY KEY(idStudent)
)ENGINE = INNODB;

CREATE OR REPLACE TABLE Subjects(
  idSubject int NOT NULL,
  name varchar(16) NOT NULL,

  PRIMARY KEY(idSubject)
)ENGINE = INNODB;

CREATE OR REPLACE TABLE Grades(
  idStudent int NOT NULL,
  grade int NOT NULL,
  idSubject int NOT NULL,
  idGrade int NOT NULL AUTO_INCREMENT,
  
  PRIMARY KEY(idGrade)
)ENGINE = INNODB;
