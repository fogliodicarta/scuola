query A) SELECT nome,cognome,nazione from tblArtisti ORDER BY cognome,nome;

query B) SELECT tblOpere.titolo,tblOpere.annoFine FROM tblOpere 
INNER JOIN tblArtisti ON tblOpere.idArtista = tblArtisti.idArtista 
WHERE tblArtisti.nome="Tiziano" AND tblArtisti.cognome="Vecellio";

query C)SELECT DISTINCT  tblSale.sala FROM tblSale JOIN(tblOpere,tblArtisti) ON (tblSale.idSala=tblOpere.idSala AND tblOpere.idArtista=tblArtisti.idArtista)  WHERE tblArtisti.nome='Vincent' AND tblArtisti.cognome='Van Gogh';


query D) SELECT DISTINCT tblSale.sala FROM tblSale JOIN(tblOpere, tblArtisti, tblCorrenteArtistica) ON (tblOpere.idSala= tblSale.idSala AND tblOpere.idArtista = tblArtisti.idArtista AND tblArtisti.idCorrenteArtistica = tblCorrenteArtistica.idCorrenteArtistica) WHERE tblCorrenteArtistica.correnteArtistica='impressionismo';

query E) SELECT DISTINCT tblArtisti.nome, tblArtisti.dataNascita, tblArtisti.dataMorte, tblNazione.nazione FROM tblArtisti JOIN(tblNazione,tblOpere, tblSale) ON (tblArtisti.idNazione= tblNazione.idNazione AND tblOpere.idArtista=tblArtisti.idArtista AND tblOpere.idSala = tblSale.idSala) WHERE tblSale.sala= "Italia1" ORDER BY tblArtisti.cognome, tblArtisti.nome;

query F) SELECT tblOpere.titolo, tblOpere.annoFine, tblOpere.tecnica, tblOpere.dimensioni FROM tblOpere JOIN (tblArtisti, tblCorrenteArtistica) ON(tblOpere.idArtista = tblArtisti.idArtista AND tblArtisti.idCorrenteArtistica = tblCorrenteArtistica.idCorrenteArtistica) WHERE tblCorrenteArtistica.correnteArtistica = 'Rinascimento' ORDER BY tblOpere.titolo;

query G)SELECT DISTINCT sala FROM tblSale JOIN(tblOpere, tblArtisti, tblCorrenteArtistica) ON (tblOpere.idSala=tblSale.idSala AND tblOpere.idArtista=tblArtisti.idArtista AND tblArtisti.idCorrenteArtistica = tblCorrenteArtistica.idCorrenteArtistica) WHERE tblCorrenteArtistica.correnteArtistica = 'Impressionismo' ORDER BY tblSale.sala;


query H) SELECT DISTINCT tblArtisti.nome, tblArtisti.cognome, COUNT( DISTINCT tblSale.Sala) AS sale FROM tblSale JOIN(tblOpere, tblArtisti) ON(tblSale.idSala=tblOpere.idSala AND tblOpere.idArtista= tblArtisti.idArtista) GROUP BY(tblArtisti.idArtista) ORDER BY tblArtisti.cognome,tblArtisti.nome;










