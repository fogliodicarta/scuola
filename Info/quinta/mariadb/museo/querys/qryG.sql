SELECT DISTINCT sala FROM tblSale 
JOIN(tblOpere, tblArtisti, tblCorrenteArtistica) ON (tblOpere.idSala=tblSale.idSala AND tblOpere.idArtista=tblArtisti.idArtista AND tblArtisti.idCorrenteArtistica = tblCorrenteArtistica.idCorrenteArtistica) 
WHERE tblCorrenteArtistica.correnteArtistica = 'Impressionismo' ORDER BY tblSale.sala;
