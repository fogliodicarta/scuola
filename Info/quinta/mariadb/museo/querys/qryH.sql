SELECT DISTINCT tblArtisti.nome, tblArtisti.cognome, COUNT( DISTINCT tblSale.sala) AS sale FROM tblSale 
JOIN(tblOpere, tblArtisti) ON(tblSale.idSala=tblOpere.idSala AND tblOpere.idArtista= tblArtisti.idArtista) 
GROUP BY(tblArtisti.idArtista) 
ORDER BY tblArtisti.cognome,tblArtisti.nome;
