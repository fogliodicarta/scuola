SELECT tblOpere.titolo, tblOpere.annoFine, tblOpere.tecnica, tblOpere.dimensioni FROM tblOpere 
JOIN (tblArtisti, tblCorrenteArtistica) ON(tblOpere.idArtista = tblArtisti.idArtista AND tblArtisti.idCorrenteArtistica = tblCorrenteArtistica.idCorrenteArtistica) 
WHERE tblCorrenteArtistica.correnteArtistica = 'Rinascimento' ORDER BY tblOpere.titolo;
