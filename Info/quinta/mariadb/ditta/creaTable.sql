CREATE TABLE dipendenti(
  ssn char(4) NOT NULL UNIQUE,
  nome varchar(255) NOT NULL,
  ufficio int NOT NULL,
  idCapo int,
  id int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY(id),
  CONSTRAINT FOREIGN KEY (idCapo) references dipendenti (id)
);
