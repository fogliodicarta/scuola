package notemusicali2;

import java.util.Scanner;

public class NoteMusicali2 {

	public static void main(String[] args) {
		int toni[] = new int[]{2, 2, 1, 2, 2, 2, 1};
		String scala[] = new String[]{"do", "do#", "re", "re#", "mi", "fa", "fa#", "sol", "sol#", "la", "la#", "si"};
		String tipo[] = new String[] {"ionica", "dorica", "frigia", "lidia", "misolida", "eolia", "locria"};
		int k = -1;
		String nota;
		Scanner tast = new Scanner(System.in);
		int puntatipo=3;
		System.out.println("Dimmi una nota musicale");
		nota = tast.nextLine();
		for (int i = 0; i < 12; i++) {
			if (nota.equals(scala[i])) {
				k = i;
			}
		}
		if (k != -1) {
			for (int i = 0; i < 7; i++) {
				System.out.println(scala[k]);
				k += toni[i];
				k %= 12;
				if (i>7) {
					i -= 7;
				}
			}
			System.out.println(nota);

		} else {
			System.out.println("Errore! Va inserita una nota musicale!");
		}
	}

}

	

